package com.okser.android.thermostatx2.view;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

public class MainActivity extends FragmentActivity {
    //private int mFragmentCount;
    //private Fragment mCurrentFragment;
    public RequestQueue mRequestQueue;
    public SharedPreferences setting;
    public boolean firstTime;


    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        Fragment f = fragmentManager.findFragmentById(android.R.id.widget_frame);
        if (f instanceof ConferenceFragment) {
            if (!((ConferenceFragment) f).doBackAction()) {
                return;
            }
        }
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            return;
        }
        super.onBackPressed();
    }
/*
    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        //mCurrentFragment = fragment;
    }
*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setting = getSharedPreferences("CWS_THERMOSTAT", 0);

        if (setting.getBoolean("First_Time", true)) {
            firstTime = true;
            setting.edit().putBoolean("First_Time", false).apply();
        } else {
            firstTime = false;
        }
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setId(android.R.id.widget_frame);
        setContentView(frameLayout);
        //getSupportFragmentManager().addOnBackStackChangedListener(this);
        mRequestQueue = Volley.newRequestQueue(this);
        //VolleyLog.DEBUG = true;
        //System.setProperty("http.keepAlive", "false");
        switchPage(new LocationsFragment(), false);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            //mRequestQueue.cancelAll(TAG);
            mRequestQueue.cancelAll(new RequestQueue.RequestFilter() {
                                        @Override
                                        public boolean apply(final Request<?> request) {
                                            return true;
                                        }
                                    }
            );
        }
    }

    public void switchPage(Fragment fragment, boolean add) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(android.R.id.widget_frame, fragment);

        if (add) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
        //fragmentManager.executePendingTransactions();

    }
/*
    @Override
    public void onBackStackChanged() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        mFragmentCount = fragmentManager.getBackStackEntryCount();
    }
    */
}
