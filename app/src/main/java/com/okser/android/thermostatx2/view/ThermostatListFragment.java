package com.okser.android.thermostatx2.view;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.adapter.ThermostatAdapter;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.LocationValue;
import com.okser.android.thermostatx2.model.ThermostatValue;

import com.okser.android.thermostatx2.utils.NetworkUtil;

import java.util.List;

public class ThermostatListFragment extends Fragment implements View.OnClickListener{
    private ListView mListView;
    private int mPos;
    private LocationValue mLocationValue;
    private ThermostatAdapter mThermostatAdapter;
    private Handler mHandler;
    private PopupWindow mPopupWindow;


    public static ThermostatListFragment newInstance(final String name, final int position) {
        ThermostatListFragment f = new ThermostatListFragment();
        Bundle args = new Bundle();
        args.putString(Consts.KEY_NAME, name);
        args.putInt(Consts.KEY_ID, position);
        f.setArguments(args);
        return f;
    }



    private void showInfo() {
        hidePopup();
        View view = View.inflate(getActivity(), R.layout.popup, null);
        //mPopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        mPopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());

        mPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                mPopupWindow.dismiss();
                return true;
            }
        });
        mPopupWindow.showAtLocation(mListView, Gravity.CENTER, 0, 0);

    }

    private void hidePopup() {
        if (mPopupWindow != null) {
            mPopupWindow.dismiss();
            mPopupWindow = null;
        }
    }

    @Override
    public void onClick(final View v) {
        int id = v.getId();
        if (id == R.id.btnleft) {
            this.getFragmentManager().popBackStack();
        } else if (id == R.id.btnright) {
            Bundle args = getArguments();
            if (args != null) {
                NewThermostatFragment f = NewThermostatFragment.newInstance(mPos);
                f.setDeviceFoundCallBack(mLocationValue);
                ((MainActivity) getActivity()).switchPage(f, true);
            }
        } else if(id == R.id.temperature) {
            v.setEnabled(false);
            if (NetworkUtil.showNetworkDialog(ThermostatListFragment.this)) {
                Integer tag = (Integer) v.getTag();
                ((MainActivity) getActivity()).switchPage(ConferenceFragment.newInstance(mPos, tag), true);
            } else {
                v.setEnabled(true);
            }
        } else if(id == R.id.ic_info) {
            // showInfo();
            Integer tag = (Integer) v.getTag();
            ((MainActivity) getActivity()).switchPage(ThermostatInfoFragment.newInstance(mPos, tag), true);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listview, container, false);
        mListView = (ListView) view.findViewById(R.id.listview);
        Bundle args = getArguments();

        if (args != null) {
            mPos = args.getInt(Consts.KEY_ID);
            mLocationValue = SnappyDBService.getInstance(getActivity()).getLocationValue(mPos);
        }
        mThermostatAdapter = new ThermostatAdapter(mLocationValue.getmDeviceValue(), getActivity(), this);
        mListView.setAdapter(mThermostatAdapter);
        int bgSize = (int)(Resources.getSystem().getDisplayMetrics().heightPixels * 0.7);
        ImageView bg = (ImageView) view.findViewById(R.id.new_thermostat_bg);
        bg.getLayoutParams().height = bgSize;
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(R.string.title_thermostats);
        View left = view.findViewById(R.id.btnleft);
        left.setOnClickListener(this);
        ImageButton right = (ImageButton) view.findViewById(R.id.btnright);
        right.setImageResource(R.drawable.icon_search);
        double elementHeight = Resources.getSystem().getDisplayMetrics().heightPixels *0.10;
        right.getLayoutParams().height = (int)elementHeight;
        right.getLayoutParams().width = (int) elementHeight;
        right.setOnClickListener(this);

        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView.requestFocus();
    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler.postDelayed(mRunnable, 5000);
        //mRunnable.run();
        /*
        if(mLocationValue.getCount() == 0) {
            NewThermostatFragment f = NewThermostatFragment.newInstance(mPos);
            f.setDeviceFoundCallBack(mLocationValue);
            ((MainActivity) getActivity()).switchPage(f, true);
        } else {
            mRunnable.run();
        }
        */
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunnable);
        mLocationValue.onPause();
    }

    @Override
    public void onDestroyView() {
        mHandler.removeCallbacks(mRunnable);
        mListView.setAdapter(null);
        if (mThermostatAdapter != null) {
            mThermostatAdapter.destory();
            mThermostatAdapter = null;
        }
        super.onDestroyView();
    }

    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            List<ThermostatValue> mDeviceValue = mLocationValue.getmDeviceValue();

            Log.i("ThermostatListFragment", "run");

            for(ThermostatValue t: mDeviceValue) {
                t.onUpdate(new ThermostatValue.DataSetChangedListener() {
                    @Override
                    public void DataSetChanged(ChangedType c, boolean configUpdate) {
                        if(c != ChangedType.NONE && mThermostatAdapter != null) {
                            mThermostatAdapter.notifyDataSetChanged();
                        }

                    }
                });
            }
            /*
            mThermostatAdapter.onUpdate(new ThermostatValue.DataSetChangedListener() {
                @Override
                public void DataSetChanged(ChangedType c, boolean configUpdate) {
                    if (mThermostatAdapter != null) {
                        mThermostatAdapter.notifyDataSetChanged();
                    }

                }
            });
            */
            mHandler.postDelayed(mRunnable, 10000);
        }
    };
}
