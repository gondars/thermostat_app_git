package com.okser.android.thermostatx2.consts;

public enum ProgramType {
    PROGRAMTYPE52, PROGRAMTYPE61, PROGRAMTYPE7;
    private static final ProgramType values[] = values();
    private static final int size = values().length;

    public static ProgramType get(int num) {
        if(num >= size) {
            num = 0;
        }
        return values[num];
    }
}
