//
//  Copyright (c) 2014 Texas Instruments. All rights reserved.
//

package com.okser.android.thermostatx2.consts;

public interface SmartConfigConstants {
    String NETWORK_CHANGE_BROADCAST_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
}
