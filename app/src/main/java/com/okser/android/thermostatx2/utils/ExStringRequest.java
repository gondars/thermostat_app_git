package com.okser.android.thermostatx2.utils;

import android.text.Html;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import android.os.Build.VERSION;

public class ExStringRequest extends StringRequest {
    public ExStringRequest(final String url, final Response.Listener<String> listener, final Response.ErrorListener errorListener) {
        super(Request.Method.GET, url, listener, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        Log.i(this.getClass().getSimpleName(), "Sequence:" + getSequence() + " response:" + response.headers);
        Log.i(this.getClass().getSimpleName(), "response:" + response.statusCode);

        //Log.i(this.getClass().getSimpleName(), "html:" + Html.fromHtml(new String(response.data)).toString());
        String htmlContent;
        if (VERSION.SDK_INT >= 24) {
            htmlContent = Html.fromHtml(new String(response.data), Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            htmlContent = Html.fromHtml(new String(response.data)).toString();
        }
        int index = htmlContent.indexOf("{");
        int endindex = htmlContent.lastIndexOf("}");
        String realContent = htmlContent.substring(index + 1, endindex);
        String newresponse = realContent.replace("\"", "").replace("\t", "");
        //Log.i(this.getClass().getSimpleName(), "real:" + realContent);
        return Response.success(newresponse, HttpHeaderParser.parseCacheHeaders(response));
    }
}
