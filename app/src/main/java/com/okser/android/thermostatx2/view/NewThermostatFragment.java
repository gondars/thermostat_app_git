package com.okser.android.thermostatx2.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.consts.SmartConfigConstants;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.Device;
import com.okser.android.thermostatx2.model.Location;
import com.okser.android.thermostatx2.utils.MDnsCallbackInterface;
import com.okser.android.thermostatx2.utils.MDnsTask;
import com.okser.android.thermostatx2.utils.NetworkUtil;
import com.okser.android.thermostatx2.utils.ThermostatUpdate;

public class NewThermostatFragment extends Fragment implements MDnsCallbackInterface, View.OnClickListener {
    private RelativeLayout mBtnSearch;
    private AlertDialog mDeviceFondDialog;
    private TextView searchingTxt;
    private MDnsTask mDnsTask;
    private TextView mEditWifiName;
    private EditText mEditWifiPassword;
    private Location mLocation;
    private ThermostatUpdate mTstatUpdate;
    private PopupWindow mPopupWindow;


    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (SmartConfigConstants.NETWORK_CHANGE_BROADCAST_ACTION.equals(action)) {
                mEditWifiName.setText(NetworkUtil.getWifiName(getActivity()));
                int networkState = NetworkUtil.getConnectionStatus(context);
                if (networkState != NetworkUtil.WIFI) {
                    if (mDnsTask != null) {
                        stopScanDevice();
                    }
                }
            }

        }
    };
    private AlertDialog mWifiDialog;

    public static NewThermostatFragment newInstance(int pos) {

        Bundle args = new Bundle();
        args.putInt(Consts.KEY_ID, pos);
        NewThermostatFragment f = new NewThermostatFragment();
        f.setArguments(args);
        return f;
    }

    public void setDeviceFoundCallBack(ThermostatUpdate callBack) {
        mTstatUpdate = callBack;
    }

    private void activateWifi() {
        WifiManager wifiManager = (WifiManager) getActivity().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled())
            wifiManager.setWifiEnabled(true);
        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add, container, false);

        int bgSize = (int)(Resources.getSystem().getDisplayMetrics().heightPixels * 0.7);
        ImageView bg = (ImageView) view.findViewById(R.id.new_thermostat_bg);

        RelativeLayout topBar = (RelativeLayout) view.findViewById(R.id.titlebar);
        topBar.setBackgroundColor(Color.parseColor("#3f1300"));

        bg.getLayoutParams().height = bgSize;
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(R.string.title_thermostat_add);
        View right = view.findViewById(R.id.btnright);
        right.setVisibility(View.GONE);
        View left = view.findViewById(R.id.btnleft);
        left.setOnClickListener(this);

        Bundle args = getArguments();
        if (args != null) {
            int pos = args.getInt(Consts.KEY_ID);
            mLocation = SnappyDBService.getInstance(getActivity()).getLocation(pos);
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(SmartConfigConstants.NETWORK_CHANGE_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver, filter);
        searchingTxt = (TextView) view.findViewById(R.id.searching_txt);
        mEditWifiName = ((EditText) view.findViewById(R.id.editwifi));
        mEditWifiName.setText(NetworkUtil.getWifiName(getActivity()));
        mEditWifiName.setEnabled(false);
        mEditWifiName.setTextColor(Color.WHITE);
        mEditWifiPassword = ((EditText) view.findViewById(R.id.editpwd));
        mEditWifiPassword.setText(mLocation.getWifiPassword());
        mEditWifiPassword.setTextColor(Color.WHITE);
        mBtnSearch = ((RelativeLayout) view.findViewById(R.id.btnsearch_new));
        mBtnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDnsTask != null) {
                    stopScanDevice();
                } else {
                    int networkState = NetworkUtil.getConnectionStatus(getActivity());
                    if (networkState != NetworkUtil.WIFI) {
                        showWifiDialog(getActivity());
                    } else {
                        final String wifiname = mEditWifiName.getText().toString().trim();
                        final String password = mEditWifiPassword.getText().toString().trim();
                        final String gateway = NetworkUtil.getGateway(getActivity());
                        if (TextUtils.isEmpty(wifiname)) {
                            mEditWifiName.setError(getString(R.string.emptyedit));
                            return;
                        }
                        if (TextUtils.isEmpty(password)) {
                            mEditWifiPassword.setError(getString(R.string.emptyedit));
                            return;
                        }
                        if (TextUtils.isEmpty(gateway)) {
                            Log.e(this.getClass().getSimpleName(), "gateway is null");
                            return;
                        }
                        startScanDevice(wifiname, password, gateway);
                    }
                }
            }
        });
        return view;
    }

    private synchronized void startScanDevice(final String name, final String password, final String gateway) {
        if (mDnsTask == null) {
            TextView txt = (TextView)mBtnSearch.findViewById(R.id.searchbtntext);
            txt.setText(R.string.smartconfig_cancelsearch);
            mBtnSearch.setEnabled(true);
            mDnsTask = new MDnsTask(getActivity(), this, name, password, gateway);
            mDnsTask.startScan(false);
            searchingTxt.setVisibility(View.VISIBLE);
        }
    }

    private synchronized void stopScanDevice() {
        if (mDnsTask != null) {
            mBtnSearch.setEnabled(false);
            TextView txt = (TextView)mBtnSearch.findViewById(R.id.searchbtntext);
            txt.setText(R.string.smartconfig_search);
            mDnsTask.stopScan();
            mDnsTask = null;
            searchingTxt.setVisibility(View.INVISIBLE);
        }
    }

    public void showWifiDialog(final Context context) {
        Activity act = getActivity();
        if (act != null) {
            act.runOnUiThread(new Runnable() {

                @Override
                public void run() {
//                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                    builder.setTitle(R.string.dialog_nowifi_title);
//                    builder.setMessage(R.string.dialog_nowifi_desp);
//                    builder.setPositiveButton(R.string.btn_ok, null);
//                    builder.setCancelable(false);
//                    mWifiDialog = builder.create();
//                    mWifiDialog.show();
                    showPopup(R.string.dialog_nowifi_title, R.string.dialog_nowifi_desp);
                }
            });
        }
    }


    @Override
    public void onDestroyView() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditWifiName.getWindowToken(), 0);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
        stopScanDevice();
        super.onDestroyView();
    }

    @Override
    public void onMDnsStarted() {
        Log.i(this.getClass().getSimpleName(), "onMDnsStarted");
    }

    @Override
    public void onMDnsDeviceFound(Device device) {
        if(mTstatUpdate != null) {
            mTstatUpdate.onDeviceFound(device);
        }
    }

    @Override
    public void onMDnsStoped(final boolean deviceFound) {
        Activity act = getActivity();
        if (act != null) {
            act.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if (deviceFound) {
                        mLocation.setWifiPassword(mEditWifiPassword.getText().toString().trim());
                        mLocation.setSsid(mEditWifiName.getText().toString().trim());
                        showDeviceFoundDialog();
                    } else {
                        showDeviceNotFoundDialog();
                    }

                    mDnsTask = null;
                    TextView txt = (TextView)mBtnSearch.findViewById(R.id.searchbtntext);
                    txt.setText(R.string.smartconfig_search);
                    mBtnSearch.setEnabled(true);
                }
            });
        }
    }

    private void showDeviceFoundDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(R.string.dialog_success);
//        builder.setMessage(R.string.dialog_devicefound_desp);
//        builder.setPositiveButton(R.string.btn_ok, new OnClickListener() {
//            @Override
//            public void onClick(final DialogInterface dialog, final int which) {
//                getFragmentManager().popBackStack();
//            }
//        });
//        mDeviceFondDialog = builder.create();
//        mDeviceFondDialog.show();

        showPopup(R.string.dialog_success, R.string.title_thermostat_add);
        searchingTxt.setVisibility(View.INVISIBLE);
    }

    private void showDeviceNotFoundDialog() {
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(R.string.dialog_devicenotfound_title);
//        builder.setMessage(R.string.dialog_devicenotfound_desp);
//        builder.setPositiveButton(R.string.btn_ok, new OnClickListener() {
//            @Override
//            public void onClick(final DialogInterface dialog, final int which) {
//                getFragmentManager().popBackStack();
//            }
//        });
//        mDeviceFondDialog = builder.create();
//        mDeviceFondDialog.show();
        showPopup(R.string.dialog_devicenotfound_title, R.string.dialog_devicenotfound_desp);
        searchingTxt.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(final View v) {
        getFragmentManager().popBackStack();
    }

    private void showPopup(@StringRes int title, @StringRes int text){
        if (mPopupWindow != null) {
            mPopupWindow.dismiss();
            mPopupWindow = null;
        }

        View view = View.inflate(getActivity(), R.layout.thermostatalertpopup, null);
        double popUpWidth = Resources.getSystem().getDisplayMetrics().widthPixels * 0.75;
        double popUpHeight = popUpWidth / 1.58;
        mPopupWindow = new PopupWindow(view, (int)popUpWidth, (int)popUpHeight, true);
        mPopupWindow.showAtLocation(getView(), Gravity.CENTER, 0, (int)popUpHeight / 2);

//        View popupBg = view.findViewById(R.id.popup_bg);
//        popupBg.getLayoutParams().height = (int)
        TextView popupTitle = (TextView) view.findViewById(R.id.popup_title);
        popupTitle.getLayoutParams().width = (int)popUpWidth;
        popupTitle.getLayoutParams().height = (int)(popUpHeight / 3);
        popupTitle.setTextSize((int)(popUpHeight / 10));
        popupTitle.setText(title);

        TextView popupText = (TextView) view.findViewById(R.id.popup_text);
        popupText.getLayoutParams().width = (int)popUpWidth;
        popupText.getLayoutParams().height = (int)(popUpHeight / 5);
        //popupText.setTextSize((int)(popUpHeight / 12));
        popupText.setText(text);

        Button popupBtn = (Button) view.findViewById(R.id.popup_btn);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                (int)popUpWidth / 4, (int)popUpHeight / 4);
        params.setMargins(0, (int)popUpHeight / 15, 0 ,0 );
        params.addRule(RelativeLayout.BELOW, R.id.popup_text);
        params.addRule(RelativeLayout.CENTER_HORIZONTAL);

        popupBtn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                mPopupWindow.dismiss();
                getFragmentManager().popBackStack();
            }
        });
        popupBtn.setLayoutParams(params);


    }
}