/*******************************************************************************
 * Copyright (c) 2009, 2014 IBM Corp.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution. 
 *
 * The Eclipse Public License is available at 
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at 
 *   http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Contributors:
 *    Dave Locke - initial API and implementation and/or initial documentation
 */


package com.okser.android.thermostatx2.mqtt_iot;

import android.content.Context;
import android.util.Log;

import com.okser.android.thermostatx2.model.Device;
import com.okser.android.thermostatx2.utils.NetworkUtil;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;

import javax.net.SocketFactory;

/**
 * A sample application that demonstrates how to use the Paho MQTT v3.1 Client API in
 * non-blocking callback/notification mode.
 *
 * It can be run from the command line in one of two modes:
 *  - as a publisher, sending a single message to a topic on the server
 *  - as a subscriber, listening for messages from the server
 *
 *  There are three versions of the sample that implement the same features
 *  but do so using using different programming styles:
 *  <ol>
 *  <li>Sample which uses the API which blocks until the operation completes</li>
 *  <li>SampleAsyncWait shows how to use the asynchronous API with waiters that block until
 *  an action completes</li>
 *  <li>SampleAsyncCallBack (this one) shows how to use the asynchronous API where events are
 *  used to notify the application when an action completes<li>
 *  </ol>
 *
 *  If the application is run with the -h parameter then info is displayed that
 *  describes all of the options / parameters.
 */
public class ThermostatMqttClient {
    private static final String TAG = ThermostatMqttClient.class.getName();

	int state = BEGIN;

	static final int BEGIN = 0;
	static final int CONNECTED = 1;
	static final int PUBLISHED = 2;
	static final int SUBSCRIBED = 3;
	static final int DISCONNECTED = 4;
	static final int FINISH = 5;
	static final int ERROR = 6;
	static final int DISCONNECT = 7;


	// Private instance variables
    private MqttAsyncClient 	client;
    private MqttConnectOptions 	conOpt;
    private String 		brokerUrl;
    private String      password;
    private String      userName;
	private boolean 	clean;
    private Device      mDevice;
    private SocketFactory mFactory;

	Throwable 			ex = null;
	Object 				waiter = new Object();
	boolean 			donext = false;


    public ThermostatMqttClient(Context context, Device device, boolean cleanSession, SocketFactory factory) throws MqttException {
        brokerUrl   = device.getMqttURL();
        clean 	    = cleanSession;
        mDevice     = device;
        mFactory    = factory;
    	//This sample stores in a temporary directory... where messages temporarily
    	// stored until the message has been delivered to the server.
    	//..a real application ought to store them somewhere
    	// where they are not likely to get deleted or tampered with
    	String tmpDir = System.getProperty("java.io.tmpdir");
    	MqttDefaultFilePersistence dataStore = new MqttDefaultFilePersistence(tmpDir);
        String MacAddress = NetworkUtil.getMacAddress(context);
        String clientID = "a:" + device.getMqttOrgID() + ":" + MacAddress.replace(":", "");
        // Construct the MqttClient instance
        String connectionURI;
        if (factory == null) {
            connectionURI = "tcp://" + this.brokerUrl + ":1883";
        } else {
            connectionURI = "ssl://" + this.brokerUrl + ":8883";
        }
        client = new MqttAsyncClient(connectionURI, clientID, dataStore);
    }

	/****************************************************************/
	/* Methods to implement the MqttCallback interface              */
	/****************************************************************/

    /**
     * Connect to the Internet of Things Foundation
     *
     * @param callbacks The IoTCallbacks object to register with the Mqtt Client
     * @param listener  The IoTActionListener object to register with the Mqtt Token.
     *
     * @return IMqttToken The token returned by the Mqtt Connect call
     *
     * @throws MqttException
     */
    public IMqttToken connect(IoTCallbacks callbacks, IoTActionListener listener) throws MqttException {
        // Log.d(TAG, ".connectDevice() entered");
        password = mDevice.getMqttAuthToken();
        userName = mDevice.getMqttApiKey();
        if(password == null || userName == null) {
            return null;
        }
        // Construct the object that contains connection parameters
        // such as cleanSession and LWT
        conOpt = new MqttConnectOptions();
        conOpt.setCleanSession(clean);
        conOpt.setPassword(password.toCharArray());
        conOpt.setUserName(userName);
        if (mFactory != null) {
            conOpt.setSocketFactory(mFactory);
        }

        client.setCallback(callbacks);

        if (!client.isConnected()) {
            //Log.d(TAG, "Connecting to server: " + connectionURI);
            try {
                // Connect using a non-blocking connect
                return client.connect(conOpt, "Connect", listener);
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    /**
     * Disconnect the device from the Internet of Things Foundation
     *
     * @param listener  The IoTActionListener object to register with the Mqtt Token.
     *
     * @return IMqttToken The token returned by the Mqtt Disconnect call
     *
     * @throws MqttException
     */
    public IMqttToken disconnectDevice(IoTActionListener listener) throws MqttException {
        Log.d(TAG, ".disconnectDevice() entered");
        if (client.isConnected()) {
            try {
                return client.disconnect("Disconnect", listener);
            } catch (MqttException e) {
                Log.e(TAG, "Exception caught while attempting to disconnect from server", e.getCause());
                throw e;
            }
        }
        return null;
    }

    /**
     * Subscribe to an MQTT topic
     *
     * @param topic         The MQTT topic string to subscribe to
     * @param qos           The Quality of Service to use for the subscription
     * @param listener      The IoTActionListener object to register with the Mqtt Token
     *
     * @return IMqttToken The token returned by the Mqtt Subscribe call
     *
     * @throws MqttException
     */
    public IMqttToken subscribe(String topic, int qos, IMqttActionListener listener) throws MqttException {
        Log.d(TAG, ".subscribe() entered");
        if (client.isConnected()) {
            try {
                String userContext = "Subscribe " + topic;
                return client.subscribe(topic, qos, userContext, listener);
            } catch (MqttException e) {
                Log.e(TAG, "Exception caught while attempting to subscribe to topic " + topic, e.getCause());
                throw e;
            }
        }
        return null;
    }

    /**
     * Unsubscribe from an MQTT topic
     *
     * @param topic         The MQTT topic string to unsubscribe from
     * @param listener      The IoTActionListener object to register with the Mqtt Token
     *
     * @return IMqttToken The token returned by the Mqtt Unsubscribe call
     *
     * @throws MqttException
     */
    public IMqttToken unsubscribe(String topic, IMqttActionListener listener) throws MqttException {
        Log.d(TAG, ".unsubscribe() entered");
        if (client.isConnected()) {
            try {
                String userContext = "Unsubscribe " + topic;
                return client.unsubscribe(topic, userContext, listener);
            } catch (MqttException e) {
                Log.e(TAG, "Exception caught while attempting to subscribe to topic " + topic, e.getCause());
                throw e;
            }
        }
        return null;
    }

    /**
     * Publish to an MQTT topic
     *
     * @param topic     The MQTT topic string to publish the message to
     * @param payload   The payload to be sent
     * @param qos       The Quality of Service to use when publishing the message
     * @param retained  The flag to specify whether the message should be retained
     * @param listener  The IoTActionListener object to register with the Mqtt Token
     *
     * @return IMqttDeliveryToken The token returned by the Mqtt Publish call
     *
     * @throws MqttException
     */
    public IMqttDeliveryToken publish(String topic, String payload, int qos, boolean retained, IoTActionListener listener) throws MqttException {
        Log.d(TAG, ".publish() entered");

        // check if client is connected
        if (client.isConnected()) {
            // create a new MqttMessage from the message string
            MqttMessage mqttMsg = new MqttMessage(payload.getBytes());
            // set retained flag
            mqttMsg.setRetained(retained);
            // set quality of service
            mqttMsg.setQos(qos);
            try {
                // create ActionListener to handle message published results
                Log.d(TAG, ".publish() - Publishing " + payload + " to: " + topic + ", with QoS: " + qos + " with retained flag set to " + retained);
                String userContext = "Publish " + topic;
                return client.publish(topic, mqttMsg, userContext, listener);
            } catch (MqttPersistenceException e) {
                Log.e(TAG, "MqttPersistenceException caught while attempting to publish a message", e.getCause());
                throw e;
            } catch (MqttException e) {
                Log.e(TAG, "MqttException caught while attempting to publish a message", e.getCause());
                throw e;
            }
        }
        return null;
    }
}
