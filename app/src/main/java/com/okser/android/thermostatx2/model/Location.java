package com.okser.android.thermostatx2.model;

import android.text.TextUtils;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Location implements Serializable {
    private final List<Device> mDevices;
    private int mIconId;
    private String mName;
    private String wifiPassword;
    private String ssid;

    public String getWifiPassword() {
        return wifiPassword;
    }

    public void setWifiPassword(String wifiPassword) {
        this.wifiPassword = wifiPassword;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public Location() {
        mDevices = new ArrayList<>();
        mIconId = R.drawable.popup_icon1;
    }

    public int getCount() {
        return mDevices.size();
    }

    public List<Device> getDevices() {
        return mDevices;
    }

    public int getIconId() {
        return mIconId;
    }

    public void setIconId(final int iconId) {
        mIconId = iconId;
    }

    public String getName() {
        if (TextUtils.isEmpty(mName) || mName.equals(Consts.STR_UNKNOWN_TOKEN)) {
            if(TextUtils.isEmpty(ssid)) {
                return "Location";
                // return mContext.getString(R.string.default_location);
            } else {
                return ssid;
            }
        }
        return mName;
    }

    public void setName(final String name) {
        mName = name;
    }

    public Device addDevice(Device device) {
        for (Device i : mDevices) {
            if (i.equals(device)) {
                // Update IP address in case it is changed.
                i.update(device);
                return i;
            }
        }
        mDevices.add(device);
        return device;
    }
}
