package com.okser.android.thermostatx2.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.Location;
import com.okser.android.thermostatx2.model.LocationValue;
import com.okser.android.thermostatx2.model.ThermostatValue;
import com.okser.android.thermostatx2.utils.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationAdapter extends BaseAdapter implements TextView.OnEditorActionListener  {
    private final View.OnClickListener mClickListener;
    private List<Location> mLocationList;
    private boolean mEdited;
    private SnappyDBService mDB;
    private Context mContext;

    public LocationAdapter(final Context ctx, final View.OnClickListener clickListener) {
        mDB = SnappyDBService.getInstance(ctx);
        mLocationList = mDB.getLocationList();
        mClickListener = clickListener;
        mContext = ctx;
        mEdited = false;
    }

    @Override
    public int getCount() {
        if (mLocationList != null) {
            return mLocationList.size();
        }
        return 0;
    }

    @Override
    public Location getItem(final int position) {
        if (mLocationList != null) {
            return mLocationList.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(final int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            convertView = View.inflate(parent.getContext(), R.layout.item_location, null);
        }
        int padding = (int)(Resources.getSystem().getDisplayMetrics().widthPixels * 0.05);
        convertView.setPadding(padding, 0, padding, 0);
        double elementHeight = Resources.getSystem().getDisplayMetrics().heightPixels *0.09;

        final EditText main = (EditText) convertView.findViewById(R.id.lefttext);
        final TextView sub = (TextView) convertView.findViewById(R.id.righttext);
        final ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
        final ImageView next = (ImageView) convertView.findViewById(R.id.ic_next);
        final CheckBox delcheck = (CheckBox) convertView.findViewById(R.id.delCheck);

        final Location location = getItem(position);
        main.setText(location.getName());

        sub.setText(String.valueOf(location.getCount()));
        icon.setOnClickListener(mClickListener);
        sub.setOnClickListener(mClickListener);
        next.setOnClickListener(mClickListener);
        delcheck.setOnClickListener(mClickListener);
        delcheck.setChecked(mDB.isLocationChecked(position));

        main.setEnabled(false);
        main.setTag(position);
        main.setOnEditorActionListener(this);

        icon.setImageResource(location.getIconId());
        icon.setTag(position);
        next.setTag(position);
        sub.setTag(position);
        delcheck.setTag(position);

        sub.setText(String.valueOf(location.getCount()));

        icon.getLayoutParams().height = (int)elementHeight;
        icon.getLayoutParams().width = (int)elementHeight;

        next.getLayoutParams().height = (int)(elementHeight*.7);
        next.getLayoutParams().width = (int)(elementHeight*.40);

        main.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)(elementHeight*.6));

        sub.setTextSize(TypedValue.COMPLEX_UNIT_PX, (int)(elementHeight*.6));
        return convertView;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        final Integer position = (Integer) v.getTag();
        if(actionId == EditorInfo.IME_ACTION_DONE) {
            final String s = v.getText().toString();
            final Location location = getItem(position);
            if (!s.equals(location.getName())) {
                location.setName(s);
                SnappyDBService.getInstance(mContext).updateLocation();
                final Map<String, String> params = new HashMap<>();
                params.put(Consts.KEY_LOCATION_NAME, s);
                LocationValue lv = SnappyDBService.getInstance(mContext).getLocationValue(position);
                List<ThermostatValue> tstatValueList = lv.getmDeviceValue();
                for (ThermostatValue tstatValue : tstatValueList) {
                    tstatValue.PostRequest(params, null);
                }
            }
            Utils.hideKeyboard(v.getContext(), v);
            v.clearFocus();
            return true;
        }
        return false;
    }

    public boolean isEdited() {
        return mEdited;
    }

    public void save(Context ctx) {
        mDB.updateLocation();
        mEdited = false;
    }
}
