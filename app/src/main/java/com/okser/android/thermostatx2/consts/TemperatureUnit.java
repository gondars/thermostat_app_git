package com.okser.android.thermostatx2.consts;

public enum TemperatureUnit {
    F, C;
    private static final TemperatureUnit values[] = values();
    private static final int size = values().length;

    public static TemperatureUnit get(int num) {
        if(num >= size) {
            num = 0;
        }
        return values[num];
    }
}
