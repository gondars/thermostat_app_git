package com.okser.android.thermostatx2.consts;

public enum Fan {
    Auto, On, Circulate;
    private static final Fan values[] = values();
    private static final int size = values().length;

    public static Fan get(int num) {
        if(num >= size) {
            num = 0;
        }
        return values[num];
    }
}
