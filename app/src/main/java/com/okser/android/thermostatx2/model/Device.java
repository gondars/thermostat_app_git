package com.okser.android.thermostatx2.model;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.utils.ExStringRequest;
import com.okser.android.thermostatx2.view.MainActivity;

import java.io.Serializable;

public class Device implements Serializable {
    private String mIPAddress;
    private String mMACAddress;
    private String mName;
    private String mPrivateName;
    private String ssid;
    private String wifiPassword;
    private String mqttApiKey;
    private String mqttAuthToken;
    private String mqttDeviceID;
    private String mqttDeviceType;
    private String mqttOrgID;
    private String mqttSerialNo;
    private String mqttURL;
    private String mqttFormat;

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        if (!TextUtils.isEmpty(ssid)) {
            this.ssid = ssid;
        }
    }

    public String getWifiPassword() {
        return wifiPassword;
    }

    public void setWifiPassword(String wifiPassword) {
        if (!TextUtils.isEmpty(wifiPassword)) {
            this.wifiPassword = wifiPassword;
        }
    }

    public String getMqttApiKey() {
        return mqttApiKey;
    }

    public void setMqttApiKey(String mqttApiKey) {
        this.mqttApiKey = mqttApiKey;
    }

    public String getMqttAuthToken() {
        return mqttAuthToken;
    }

    public void setMqttAuthToken(String mqttAuthToken) {
        this.mqttAuthToken = mqttAuthToken;
    }

    public String getMqttDeviceID() {
        return mqttDeviceID;
    }

    public void setMqttDeviceID(String mqttDeviceID) {
        this.mqttDeviceID = mqttDeviceID;
    }

    public String getMqttDeviceType() {
        return mqttDeviceType;
    }

    public void setMqttDeviceType(String mqttDeviceType) {
        this.mqttDeviceType = mqttDeviceType;
    }

    public String getMqttOrgID() {
        return mqttOrgID;
    }

    public void setMqttOrgID(String mqttOrgID) {
        this.mqttOrgID = mqttOrgID;
    }

    public String getMqttSerialNo() {
        return mqttSerialNo;
    }

    public void setMqttSerialNo(String mqttSerialNo) {
        this.mqttSerialNo = mqttSerialNo;
    }

    public String getMqttURL() {
        return mqttURL;
    }

    public void setMqttURL(String mqttURL) {
        this.mqttURL = mqttURL;
    }

    public String getMqttFormat() {
        return mqttFormat;
    }

    public void setMqttFormat(String mqttFormat) {
        this.mqttFormat = mqttFormat;
    }

    public String getIPAddress() {
        return mIPAddress;
    }

    public void setIPAddress(String ip) {
        if (!TextUtils.isEmpty(ip)) {
            this.mIPAddress = ip;
        }
    }

    public String getMACAddress() {
        return mMACAddress;
    }

    public void setMACAddress(String MACAddress) {
        if (!TextUtils.isEmpty(MACAddress)) {
            this.mMACAddress = MACAddress;
        }
    }

    public String getName() {
        if (TextUtils.isEmpty(mName) || mName.equals(Consts.STR_UNKNOWN_TOKEN)) {
            return "Home";
        }
        return mName;
    }

    public void setName(String name) {
        if (!(TextUtils.isEmpty(name) || name.equals(Consts.STR_UNKNOWN_TOKEN))) {
            this.mName = name;
        }
    }

    public String getPrivateName() {
        return mPrivateName;
    }

    public void setPrivateName(final String privateName) {
        mPrivateName = privateName;
    }

    public boolean equals(Device d) {
        return mMACAddress.equals(d.getMACAddress());
    }

    public void update(Device d) {
        setIPAddress(d.mIPAddress);
        setSsid(d.ssid);
        //wifiPassword = d.wifiPassword;
        //mName = d.mName;
        //mPrivateName = d.mPrivateName;
    }

    public interface MqttConfigReceivedListener {
        public void onReceived(boolean received);
    }

    public void httpGetMqttConfig(Activity activity, final MqttConfigReceivedListener listener) {

        final RequestQueue requestQueue = ((MainActivity)activity).mRequestQueue;
        final String MqttConfig = String.format(Consts.KEY_MQTT_CFG, mIPAddress);
        ExStringRequest config = new ExStringRequest(MqttConfig, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.i(Device.class.getSimpleName(), "response:" + response + " host:" + mIPAddress);
                String[] datapair = response.split(",");
                for (String strValue : datapair) {
                    if (TextUtils.isEmpty(strValue)) {
                        continue;
                    }
                    String[] keys = strValue.split(":");
                    final String key = keys[0].trim();
                    final String value = keys[1].trim();
                    if(key.equals(Consts.KEY_MQTT_API_KEY)) {
                        mqttApiKey = value;
                    } else if(key.equals(Consts.KEY_MQTT_AUTH_TOKEN)) {
                        mqttAuthToken = value;
                    } else if(key.equals(Consts.KEY_MQTT_URL)) {
                        mqttURL = value;
                    } else if(key.equals(Consts.KEY_MQTT_DEVICE_ID)) {
                        mqttDeviceID = value;
                    } else if(key.equals(Consts.KEY_MQTT_DEVICE_TYPE)) {
                        mqttDeviceType = value;
                    } else if(key.equals(Consts.KEY_MQTT_FORMAT)) {
                        mqttFormat = value;
                    } else if(key.equals(Consts.KEY_MQTT_ORG_ID)) {
                        mqttOrgID = value;
                    } else if(key.equals(Consts.KEY_MQTT_SERIAL_NO)) {
                        mqttSerialNo = value;
                    }
                }
                listener.onReceived(true);
                //errorListener.onErrorResponse(null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                listener.onReceived(false);
                Log.e(Device.class.getSimpleName(), "response: error" + error.getMessage(), error);
                //Toast.makeText(mCtx, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        config.setRetryPolicy(new DefaultRetryPolicy(
                5000, //MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(config);
    }
}
