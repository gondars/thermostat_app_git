package com.okser.android.thermostatx2.view;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.consts.ProgramType;
import com.okser.android.thermostatx2.consts.TemperatureUnit;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.ThermostatValue;
import com.okser.android.thermostatx2.utils.ITabListener;
import com.okser.android.thermostatx2.utils.Utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SetSchedFragment extends Fragment implements ITabListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener, DialogInterface.OnClickListener, TimePickerDialog.OnTimeSetListener {
    private static final int INDEX_BACK = 2;
    private static final int INDEX_COOL = 0;
    private static final int INDEX_HEAT = 1;
    private static final int INDEX_HOUR = 2;
    private static final int INDEX_LEAVE = 1;
    private static final int INDEX_MIN = 3;
    private static final int INDEX_SLEEP = 3;
    private static final int INDEX_WAKE = 0;
    private TextView mClock;
    private TextView mCoolText;
    private boolean mCurSch1;
    private TextView mFri;
    private TextView mHeatText;
    private TextView mLabelTemperature;
    private TextView mLabelTime;
    private TextView mMon;
    private int[][] mOldOptions1;
    private int[][] mOldOptions2;
    private ProgramType mOldProgramType;
    private int[][] mOptions1;
    private int[][] mOptions2;
    private TextView mSat;
    private ProgramType mProgramType;
    private TextView mScheduleDesp;
    private RadioGroup mScheduleGroup;
    private int mSetIndex;
    private TextView mSun;
    private TextView mThur;
    private View mTimeContent;
    private TextView mTus;
    private TemperatureUnit mTemperatureUnit;
    private TextView mWed;
    private ThermostatValue mTstatValue;
    private View coolup;
    private View cooldown;
    private View heatup;
    private View heatdown;

    public static SetSchedFragment newInstance(Bundle args) {
        // Bundle args = new Bundle(extras);
        SetSchedFragment f = new SetSchedFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public boolean doBackAction() {
        if (mTimeContent.getVisibility() == View.VISIBLE) {
            swtichDetailView(true, null);
            return false;
        }
        return true;
    }

    private String toString(int[][] options) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                builder.append(options[i][j]);
                builder.append(" ");
            }
        }
        builder.delete(builder.length() - 1, builder.length());
        return builder.toString();
    }

    public void doSave(final ThermostatValue.DataSetChangedListener listener) {
        if(mTstatValue == null) {
            return;
        }

        final Map<String, String> params = new HashMap<>();
        params.put(Consts.KEY_PROGRAM_TYPE, String.valueOf(mProgramType.ordinal()));
        params.put(Consts.KEY_SCHEDULE_1, toString(mOptions1));
        params.put(Consts.KEY_SCHEDULE_2, toString(mOptions2));
        mTstatValue.PostRequest(params, listener);
    }

    @NonNull
    private String saveOption(int[][] optionold, int[][] optionnew) {
        System.arraycopy(optionnew[0], 0, optionold[0], 0, optionnew[0].length);
        System.arraycopy(optionnew[1], 0, optionold[1], 0, optionnew[1].length);
        System.arraycopy(optionnew[2], 0, optionold[2], 0, optionnew[2].length);
        System.arraycopy(optionnew[3], 0, optionold[3], 0, optionnew[3].length);

        String sch = toString(optionnew);

        Log.i(this.getClass().getSimpleName(), "schedule:" + sch);
        return sch;
    }

    private void swtichDetailView(boolean hide, String title) {
        View all = getView().findViewById(R.id.radio_all);
        View oneday = getView().findViewById(R.id.radio_oneday);
        View workday = getView().findViewById(R.id.radio_workday);
        mScheduleGroup.setEnabled(hide);
        mMon.setEnabled(hide);
        mTus.setEnabled(hide);
        mWed.setEnabled(hide);
        mThur.setEnabled(hide);
        mFri.setEnabled(hide);
        mSat.setEnabled(hide);
        mSun.setEnabled(hide);
        all.setEnabled(hide);
        oneday.setEnabled(hide);
        workday.setEnabled(hide);
        View leave = getView().findViewById(R.id.leavegroup);
        View sleep = getView().findViewById(R.id.sleepgroup);
        View wake = getView().findViewById(R.id.wakegroup);
        View back = getView().findViewById(R.id.backgroup);
        if (hide) {
            mTimeContent.setVisibility(View.GONE);
            leave.setVisibility(View.VISIBLE);
            sleep.setVisibility(View.VISIBLE);
            wake.setVisibility(View.VISIBLE);
            back.setVisibility(View.VISIBLE);
            mScheduleDesp.setVisibility(View.VISIBLE);
            updateTime(getView(), mSetIndex);
            ((ConferenceFragment) getParentFragment()).setTitle(null);
        } else {
            setClock();
            mTimeContent.setVisibility(View.VISIBLE);
            leave.setVisibility(View.GONE);
            sleep.setVisibility(View.GONE);
            wake.setVisibility(View.GONE);
            back.setVisibility(View.GONE);
            mScheduleDesp.setVisibility(View.GONE);
            ((ConferenceFragment) getParentFragment()).setTitle(title);
        }
        updateView();
    }

    private void updateTime(View view, int index) {
        int[][] options = mCurSch1 ? mOptions1 : mOptions2;
        switch (index) {
            case INDEX_WAKE: {
                TextView waketime = (TextView) view.findViewById(R.id.waketime);
                TextView wakecool = (TextView) view.findViewById(R.id.wakecool);
                TextView wakeheat = (TextView) view.findViewById(R.id.wakeheat);
                setClock(waketime, options[INDEX_WAKE][INDEX_HOUR], options[INDEX_WAKE][INDEX_MIN]);
                setTemperature(wakecool, options[INDEX_WAKE][INDEX_COOL]);
                setTemperature(wakeheat, options[INDEX_WAKE][INDEX_HEAT]);
            }
            break;
            case INDEX_SLEEP: {
                TextView sleeptime = (TextView) view.findViewById(R.id.sleeptime);
                TextView sleepcool = (TextView) view.findViewById(R.id.sleepcool);
                TextView sleepheat = (TextView) view.findViewById(R.id.sleepheat);
                setClock(sleeptime, options[INDEX_SLEEP][INDEX_HOUR], options[INDEX_SLEEP][INDEX_MIN]);
                setTemperature(sleepcool, options[INDEX_SLEEP][INDEX_COOL]);
                setTemperature(sleepheat, options[INDEX_SLEEP][INDEX_HEAT]);
            }
            break;
            case INDEX_LEAVE: {
                TextView leavetime = (TextView) view.findViewById(R.id.leavetime);
                TextView leavecool = (TextView) view.findViewById(R.id.leavecool);
                TextView leaveheat = (TextView) view.findViewById(R.id.leaveheat);
                setClock(leavetime, options[INDEX_LEAVE][INDEX_HOUR], options[INDEX_LEAVE][INDEX_MIN]);
                setTemperature(leavecool, options[INDEX_LEAVE][INDEX_COOL]);
                setTemperature(leaveheat, options[INDEX_LEAVE][INDEX_HEAT]);
            }
            break;
            case INDEX_BACK: {
                TextView backtime = (TextView) view.findViewById(R.id.backtime);
                TextView backcool = (TextView) view.findViewById(R.id.backcool);
                TextView backheat = (TextView) view.findViewById(R.id.backheat);
                setClock(backtime, options[INDEX_BACK][INDEX_HOUR], options[INDEX_BACK][INDEX_MIN]);
                setTemperature(backcool, options[INDEX_BACK][INDEX_COOL]);
                setTemperature(backheat, options[INDEX_BACK][INDEX_HEAT]);
            }
            break;
        }
    }

    private void setClock(TextView hold, int hour, int minute) {
        String minText = minute < 10 ? "0" + minute : String.valueOf(minute);
        if (hour <= 12) {
            String hourText = hour < 10 ? "0" + hour : String.valueOf(hour);
            hold.setText(hourText + ":" + minText + " AM");
        } else {
            hour -= 12;
            String hourText = hour < 10 ? "0" + hour : String.valueOf(hour);
            hold.setText(hourText + ":" + minText + " PM");
        }
    }

    private void setTemperature(TextView textView, int temp) {
        Utils.setTemperature(getActivity(), textView, temp, mTemperatureUnit);
    }

    private void setClock() {
        int[][] options = mCurSch1 ? mOptions1 : mOptions2;
        setClock(mClock, options[mSetIndex][INDEX_HOUR], options[mSetIndex][INDEX_MIN]);
    }

    private void updateView() {
        boolean changed = ((!mProgramType.equals(mOldProgramType))
                || (!Arrays.equals(mOldOptions1[0], mOptions1[0]))
                || (!Arrays.equals(mOldOptions1[1], mOptions1[1]))
                || (!Arrays.equals(mOldOptions1[2], mOptions1[2]))
                || (!Arrays.equals(mOldOptions1[3], mOptions1[3]))
                || (!Arrays.equals(mOldOptions2[0], mOptions2[0]))
                || (!Arrays.equals(mOldOptions2[1], mOptions2[1]))
                || (!Arrays.equals(mOldOptions2[2], mOptions2[2]))
                || (!Arrays.equals(mOldOptions2[3], mOptions2[3])));
        Fragment f = getParentFragment();
        if (f instanceof ConferenceFragment) {
            ((ConferenceFragment) f).updateRightBtn(changed);
        }
    }

    @Override
    public void onCheckedChanged(final RadioGroup group, final int checkedId) {
        if (checkedId == R.id.radio_all) {
            mProgramType = ProgramType.PROGRAMTYPE7;
        } else if (checkedId == R.id.radio_oneday) {
            mProgramType = ProgramType.PROGRAMTYPE61;
        } else {
            mProgramType = ProgramType.PROGRAMTYPE52;
        }
        mCurSch1 = true;
        updateSchedule(getView());
    }

    private void updateSchedule(View view) {
        if (mProgramType == ProgramType.PROGRAMTYPE61) {
            mScheduleGroup.check(R.id.radio_oneday);
        } else if (mProgramType == ProgramType.PROGRAMTYPE52) {
            mScheduleGroup.check(R.id.radio_workday);
        } else {
            mScheduleGroup.check(R.id.radio_all);
        }
        updateDays();
        updateOptions(view);
        updateView();
    }

    private void updateDays() {
        int resid1;
        int resid2;
        if (mCurSch1) {
            resid1 = R.drawable.checkbox_selected;
            resid2 = R.drawable.check_off;
        } else {
            resid1 = R.drawable.check_off;
            resid2 = R.drawable.checkbox_selected;
        }
//        if(true){return;}
        mMon.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid1);
        mTus.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid1);
        mWed.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid1);
        mThur.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid1);
        mFri.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid1);
        if (mProgramType == ProgramType.PROGRAMTYPE61) {
            mSat.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid1);
            mSun.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid2);
            mScheduleDesp.setText(mCurSch1 ? R.string.setting_612 : R.string.setting_611);
            mScheduleDesp.setVisibility(View.VISIBLE);
            //TODO FIX SCREEN SWITCHING AND SHITTY POPUP
            //swtichDetailView(true, null);
        } else if (mProgramType == ProgramType.PROGRAMTYPE52) {
            mSat.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid2);
            mSun.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid2);
            mScheduleDesp.setText(mCurSch1 ? R.string.setting_522 : R.string.setting_521);
            mScheduleDesp.setVisibility(View.VISIBLE);
            //swtichDetailView(true, null);
        } else {
            mSat.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid1);
            mSun.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, resid1);
            mScheduleDesp.setVisibility(View.GONE);
        }

    }

    private void updateOptions(View view) {
        int[][] options = mCurSch1 ? mOptions1 : mOptions2;
        TextView waketime = (TextView) view.findViewById(R.id.waketime);
        TextView wakecool = (TextView) view.findViewById(R.id.wakecool);
        TextView wakeheat = (TextView) view.findViewById(R.id.wakeheat);
        setClock(waketime, options[INDEX_WAKE][INDEX_HOUR], options[INDEX_WAKE][INDEX_MIN]);
        setTemperature(wakecool, options[INDEX_WAKE][INDEX_COOL]);
        setTemperature(wakeheat, options[INDEX_WAKE][INDEX_HEAT]);

        TextView sleeptime = (TextView) view.findViewById(R.id.sleeptime);
        TextView sleepcool = (TextView) view.findViewById(R.id.sleepcool);
        TextView sleepheat = (TextView) view.findViewById(R.id.sleepheat);
        setClock(sleeptime, options[INDEX_SLEEP][INDEX_HOUR], options[INDEX_SLEEP][INDEX_MIN]);
        setTemperature(sleepcool, options[INDEX_SLEEP][INDEX_COOL]);
        setTemperature(sleepheat, options[INDEX_SLEEP][INDEX_HEAT]);

        TextView backtime = (TextView) view.findViewById(R.id.backtime);
        TextView backcool = (TextView) view.findViewById(R.id.backcool);
        TextView backheat = (TextView) view.findViewById(R.id.backheat);
        setClock(backtime, options[INDEX_BACK][INDEX_HOUR], options[INDEX_BACK][INDEX_MIN]);
        setTemperature(backcool, options[INDEX_BACK][INDEX_COOL]);
        setTemperature(backheat, options[INDEX_BACK][INDEX_HEAT]);

        TextView leavetime = (TextView) view.findViewById(R.id.leavetime);
        TextView leavecool = (TextView) view.findViewById(R.id.leavecool);
        TextView leaveheat = (TextView) view.findViewById(R.id.leaveheat);
        setClock(leavetime, options[INDEX_LEAVE][INDEX_HOUR], options[INDEX_LEAVE][INDEX_MIN]);
        setTemperature(leavecool, options[INDEX_LEAVE][INDEX_COOL]);
        setTemperature(leaveheat, options[INDEX_LEAVE][INDEX_HEAT]);
    }

    private void rangeCheck(int coolvalue, int heatvalue) {
        boolean coldDownEnabled = coolvalue > Consts.TEMP_MIN &&
                (coolvalue - 1) > heatvalue &&
                coolvalue > mTstatValue.getCoolLimit();

        boolean coldUpEnabled = coolvalue < Consts.TEMP_MAX;

        boolean heatDownEnabled = heatvalue > Consts.TEMP_MIN;

        boolean heatUpEnabled = heatvalue < Consts.TEMP_MAX &&
                (coolvalue - 1) > heatvalue &&
                heatvalue < mTstatValue.getHeatLimit();


        if(coldDownEnabled) {
            cooldown.setVisibility(View.VISIBLE);
        } else {
            cooldown.setVisibility(View.INVISIBLE);
        }
        if(coldUpEnabled) {
            coolup.setVisibility(View.VISIBLE);
        } else {
            coolup.setVisibility(View.INVISIBLE);
        }
        if(heatDownEnabled) {
            heatdown.setVisibility(View.VISIBLE);
        } else {
            heatdown.setVisibility(View.INVISIBLE);
        }
        if(heatUpEnabled) {
            heatup.setVisibility(View.VISIBLE);
        } else {
            heatup.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onClick(final View v) {
        int[] options = mCurSch1 ? mOptions1[mSetIndex] : mOptions2[mSetIndex];

        switch (v.getId()) {
            case R.id.summary:
                mCurSch1 = !mCurSch1;
                updateDays();
                updateView();
                updateOptions(getView());
                break;
            case R.id.mon:
            case R.id.tus:
            case R.id.wed:
            case R.id.thu:
            case R.id.fri:
                mCurSch1 = true;
                updateDays();
                updateView();
                updateOptions(getView());
                break;
            case R.id.sat:
                mCurSch1 = (mProgramType != ProgramType.PROGRAMTYPE52);
                updateDays();
                updateView();
                updateOptions(getView());
                break;
            case R.id.sun:
                mCurSch1 = (mProgramType == ProgramType.PROGRAMTYPE7);
                updateDays();
                updateView();
                updateOptions(getView());
                break;
            case R.id.wakegroup:
                showDetailSetting(INDEX_WAKE, getString(R.string.status_wake_info), getString(R.string.title_wakesettings));
                break;
            case R.id.sleepgroup:
                showDetailSetting(INDEX_SLEEP, getString(R.string.status_sleep_info), getString(R.string.title_sleepsettings));
                break;
            case R.id.backgroup:
                showDetailSetting(INDEX_BACK, getString(R.string.status_return_info), getString(R.string.title_backsettings));
                break;
            case R.id.leavegroup:
                showDetailSetting(INDEX_LEAVE, getString(R.string.status_leave_info), getString(R.string.title_leavesettings));
                break;
            case R.id.coolbtndown:
                if (options[INDEX_COOL] > options[INDEX_HEAT] + 1) {
                    options[INDEX_COOL]--;
                    setTemperature(mCoolText, options[INDEX_COOL]);
                    updateView();
                }
                rangeCheck(options[INDEX_COOL], options[INDEX_HEAT]);
                break;
            case R.id.coolbtnup:
                if (options[INDEX_COOL] < Consts.TEMP_MAX) {
                    options[INDEX_COOL]++;
                    setTemperature(mCoolText, options[INDEX_COOL]);
                    updateView();
                }
                rangeCheck(options[INDEX_COOL], options[INDEX_HEAT]);
                break;
            case R.id.heatbtndown:
                if (options[INDEX_HEAT] > Consts.TEMP_MIN) {
                    options[INDEX_HEAT]--;
                    setTemperature(mHeatText, options[INDEX_HEAT]);
                    updateView();
                }
                rangeCheck(options[INDEX_COOL], options[INDEX_HEAT]);
                break;
            case R.id.heatbtnup:
                if (options[INDEX_HEAT] < options[INDEX_COOL] - 1) {
                    options[INDEX_HEAT]++;
                    setTemperature(mHeatText, options[INDEX_HEAT]);
                    updateView();
                }
                rangeCheck(options[INDEX_COOL], options[INDEX_HEAT]);
                break;
            case R.id.settings_clock:
                TimePickerDialog dialog = new TimePickerDialog(getActivity(), this, options[INDEX_HOUR], options[INDEX_MIN], false);
                dialog.show();
                break;
        }
    }

    private void showDetailSetting(final int index, String name, String title) {
        mSetIndex = index;
        name = name.toLowerCase();
        mLabelTemperature.setText(String.format(getString(R.string.setting_temperature), name));
        mLabelTime.setText(String.format(getString(R.string.setting_time), name));
        int[][] options = mCurSch1 ? mOptions1 : mOptions2;
        setTemperature(mHeatText, options[index][INDEX_HEAT]);
        setTemperature(mCoolText, options[index][INDEX_COOL]);
        swtichDetailView(false, title);
    }

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
        if (which == DialogInterface.BUTTON_POSITIVE) {
            doSave(null);
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_setsched, container, false);
        mTimeContent = view.findViewById(R.id.settings);
        mScheduleGroup = (RadioGroup) view.findViewById(R.id.radio_group);
        mMon = (TextView) view.findViewById(R.id.mon);
        mTus = (TextView) view.findViewById(R.id.tus);
        mWed = (TextView) view.findViewById(R.id.wed);
        mThur = (TextView) view.findViewById(R.id.thu);
        mFri = (TextView) view.findViewById(R.id.fri);
        mSat = (TextView) view.findViewById(R.id.sat);
        mSun = (TextView) view.findViewById(R.id.sun);
        mScheduleDesp = (TextView) view.findViewById(R.id.summary);
        mLabelTime = (TextView) view.findViewById(R.id.settings_time_label);
        mLabelTemperature = (TextView) view.findViewById(R.id.settings_temp_label);
        mHeatText = (TextView) view.findViewById(R.id.heat_text_temperature);
        mCoolText = (TextView) view.findViewById(R.id.cool_text_temperature);
        mClock = (TextView) view.findViewById(R.id.settings_clock);
        mOldOptions1 = new int[4][4];
        mOldOptions2 = new int[4][4];
        mOptions1 = new int[4][4];
        mOptions2 = new int[4][4];
        mCurSch1 = true;

        View leave = view.findViewById(R.id.leavegroup);
        View sleep = view.findViewById(R.id.sleepgroup);
        View back = view.findViewById(R.id.backgroup);
        View wake = view.findViewById(R.id.wakegroup);
        coolup = view.findViewById(R.id.coolbtnup);
        cooldown = view.findViewById(R.id.coolbtndown);
        heatup = view.findViewById(R.id.heatbtnup);
        heatdown = view.findViewById(R.id.heatbtndown);

        mScheduleGroup.setOnCheckedChangeListener(this);
        mClock.setOnClickListener(this);
        coolup.setOnClickListener(this);
        cooldown.setOnClickListener(this);
        heatup.setOnClickListener(this);
        heatdown.setOnClickListener(this);
        mMon.setOnClickListener(this);
        mTus.setOnClickListener(this);
        mWed.setOnClickListener(this);
        mThur.setOnClickListener(this);
        mFri.setOnClickListener(this);
        mSat.setOnClickListener(this);
        mSun.setOnClickListener(this);
        leave.setOnClickListener(this);
        sleep.setOnClickListener(this);
        back.setOnClickListener(this);
        wake.setOnClickListener(this);
        mScheduleDesp.setOnClickListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = new Bundle(getArguments());
        mTstatValue = SnappyDBService.getInstance(getActivity()).getTstatValue(args);
        if(mTstatValue != null) {
            onUpdate(mTstatValue);
        }
    }

    public void onUpdate(ThermostatValue tstatData) {
        mTemperatureUnit = tstatData.getTemperatureUnit();
        mProgramType = tstatData.getProgramType();
        mOldProgramType = mProgramType;
        initOptions(mOldOptions1, mOptions1, tstatData.getSchedule1());
        initOptions(mOldOptions2, mOptions2, tstatData.getSchedule1());
        updateSchedule(getView());
    }

    private void initOptions(int[][] option, int[][] optionnew, String sch) {
        String[] scharr = sch.split(Consts.SP);
        for (int i = 0, length = scharr.length, index = 0; i < length; i += 4, index++) {
            option[index][INDEX_COOL] = Integer.valueOf(scharr[i]);
            option[index][INDEX_HEAT] = Integer.valueOf(scharr[i + 1]);
            option[index][INDEX_HOUR] = Integer.valueOf(scharr[i + 2]);
            option[index][INDEX_MIN] = Integer.valueOf(scharr[i + 3]);
            optionnew[index][INDEX_COOL] = option[index][INDEX_COOL];
            optionnew[index][INDEX_HEAT] = option[index][INDEX_HEAT];
            optionnew[index][INDEX_HOUR] = option[index][INDEX_HOUR];
            optionnew[index][INDEX_MIN] = option[index][INDEX_MIN];
        }
    }

    @Override
    public void onTimeSet(final TimePicker view, final int hourOfDay, final int minute) {
        int[][] options = mCurSch1 ? mOptions1 : mOptions2;
        options[mSetIndex][INDEX_HOUR] = hourOfDay;
        options[mSetIndex][INDEX_MIN] = minute;
        setClock();
        Log.i(this.getClass().getSimpleName(), "timeset:" + Arrays.toString(options[0]));
        Log.i(this.getClass().getSimpleName(), "timeset:" + Arrays.toString(options[1]));
        Log.i(this.getClass().getSimpleName(), "timeset:" + Arrays.toString(options[2]));
        Log.i(this.getClass().getSimpleName(), "timeset:" + Arrays.toString(options[3]));
        updateView();
    }
}
