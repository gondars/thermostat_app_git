package com.okser.android.thermostatx2.utils;


import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.MulticastLock;
import android.util.Log;

import com.integrity_project.smartconfiglib.SmartConfig;
import com.integrity_project.smartconfiglib.SmartConfigListener;
import com.okser.android.thermostatx2.model.Device;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceListener;
import javax.jmdns.ServiceTypeListener;

public final class MDnsTask extends Thread implements ServiceListener, ServiceTypeListener, SmartConfigListener {
    private static final String SERVICE_TYPE = "_http._tcp.";
    private static final String SMARTCONFIG_IDENTIFIER = "srcvers=1D90645";
    //private static final String SMARTCONFIG_IDENTIFIER = "mysimplelink";
    private static final String TAG = MDnsTask.class.getSimpleName();
    private static final long TIMELIMIIT = 15000;
    private final String mGateway;
    private final MDnsCallbackInterface mMdnsCallback;
    private final MulticastLock mMulticastLock;
    private final WifiManager mWifiManager;
    private final String mWifiName;
    private final String mWifiPwd;
    private JmDNS mJmdns;
    private SmartConfig mSmartConfig;
    private boolean mFoundDevice;
    private boolean mRepeat;

    public MDnsTask(final Context ctx, final MDnsCallbackInterface callback) {
        this(ctx, callback, null, null, null);
    }

    public MDnsTask(final Context ctx, final MDnsCallbackInterface callback, final String ssid, final String pwd,
                    final String gateway) {
        mMdnsCallback = callback;
        mWifiManager = (WifiManager) ctx.getSystemService(Context.WIFI_SERVICE);
        mMulticastLock = mWifiManager.createMulticastLock(getClass().getName());
        mMulticastLock.setReferenceCounted(true);
        mGateway = gateway;
        mWifiPwd = pwd;
        mWifiName = ssid;
        mFoundDevice = false;
    }

    public void startScan(boolean repeat) {
        mRepeat = repeat;
        start();
    }

    public synchronized void stopScan() {
        mRepeat = false;
        this.interrupt();
    }

    @Override
    public void onSmartConfigEvent(SmtCfgEvent arg0, Exception arg1) {
        Log.w(TAG, arg0.toString());
        Log.w(TAG, arg0.toString(), arg1);
    }

    private void RunScan() {
        if (this.mMdnsCallback != null) {
            mMdnsCallback.onMDnsStarted();
        }
        try {
            startSmartConfig();
        } catch (Exception e) {
            Log.e(TAG, "startSmartConfig fail!", e);
        }
        synchronized (this) {

            try {
                if (mSmartConfig != null) {
                    wait(15000);
                    mSmartConfig.stopTransmitting();
                    mSmartConfig = null;
                }
                final InetAddress deviceIpAddress = getDeviceIpAddress(mWifiManager);
                if (mJmdns == null) {
                    mJmdns = JmDNS.create(deviceIpAddress, "SmartConfig");
                    mJmdns.addServiceTypeListener(this);
                }
                wait(10000);
            } catch (InterruptedException e) {
                Log.w(TAG, "discover terminate!");
                //throw new InterruptedException("Discover terminate");
            } catch (Exception e) {
                Log.e(TAG, "SmartConfig fail!", e);
            } finally {
                stopMSDN();
                if (this.mMdnsCallback != null) {
                    mMdnsCallback.onMDnsStoped(mFoundDevice);
                }
            }
        }
    }

    @Override
    public void run() {
        int period = 60000;
        do {
            try {
                RunScan();
                synchronized (this) {
                    wait(period);
                }
            } catch (InterruptedException e) {

            }

        } while(mRepeat);
    }

    private void startSmartConfig() throws Exception {
        Log.i(TAG, "MDNS start discovery >> ");
        long time = System.currentTimeMillis();
        byte[] freeData = new byte[]{0x03};
        byte[] paddedEncryptionKey = null;

        if (!mMulticastLock.isHeld()) {
            mMulticastLock.acquire();
        } else {
            Log.i(TAG, " Muticast lock already held...");
        }
        Log.i(TAG, "MDNS discovery acquire " + (System.currentTimeMillis() - time));
        if(mGateway != null && mWifiPwd != null && mWifiName != null) {
            mSmartConfig = new SmartConfig(this, freeData, this.mWifiPwd, paddedEncryptionKey, this.mGateway,
                    this.mWifiName, (byte) 0, "");
            mSmartConfig.transmitSettings();
        } else {
            mSmartConfig = null;
        }

        Log.i(TAG, "MDNS start discovery << " + (System.currentTimeMillis() - time));
    }

    private InetAddress getDeviceIpAddress(WifiManager wifi) {
        InetAddress result = null;
        try {
            // default to Android localhost
            result = InetAddress.getByName("10.0.0.2");

            // figure out our wifi address, otherwise bail
            WifiInfo wifiinfo = wifi.getConnectionInfo();
            int intaddr = wifiinfo.getIpAddress();
            byte[] byteaddr = new byte[]{(byte) (intaddr & 0xff), (byte) (intaddr >> 8 & 0xff),
                    (byte) (intaddr >> 16 & 0xff), (byte) (intaddr >> 24 & 0xff)};
            result = InetAddress.getByAddress(byteaddr);
        } catch (UnknownHostException ex) {
            Log.e(TAG, String.format("getDeviceIpAddress Error: %s", ex.getMessage()));
        }

        return result;
    }

    private void stopMSDN() {
        Log.i(TAG, "MDNS stop discovery  >> ");
        long time = System.currentTimeMillis();

        try {
            if (mMulticastLock.isHeld()) {
                mMulticastLock.release();
            } else {
                Log.i(TAG, "Multicast lock already released");
            }
            if (mJmdns != null) {
                mJmdns.unregisterAllServices();
                mJmdns.close();
                mJmdns = null;
            }
        } catch (IOException e) {
            Log.e(TAG, "MDNS discovery stopped", e);
        } catch (RuntimeException e) {
            Log.e(TAG, "MDNS discovery stopped", e);
        } catch (Exception e) {
            Log.e(TAG, "MDNS discovery stopped", e);
        }
        Log.i(TAG, "MDNS stop discovery << " + (System.currentTimeMillis() - time));
    }

    @Override
    public void serviceAdded(ServiceEvent service) {
    }

    @Override
    public void serviceRemoved(ServiceEvent service) {
    }

    @Override
    public void serviceResolved(ServiceEvent service) {
        Log.i(TAG, "resolved: " + " nice:" + service.getInfo().getNiceTextString());
        String nicetext = service.getInfo().getNiceTextString();
        if (nicetext.contains(SMARTCONFIG_IDENTIFIER)) {
            String name = service.getName();
            String[] keys = name.split("@");
            Device device = new Device();
            final String ipAddress = service.getInfo().getHostAddresses()[0];
            device.setIPAddress(ipAddress);
            device.setPrivateName(name);
            device.setMACAddress(keys[0]);
            device.setName(keys[1]);
            device.setSsid(mWifiName);
            device.setWifiPassword(mWifiPwd);
            mFoundDevice = true;
            mMdnsCallback.onMDnsDeviceFound(device);
            Log.i(TAG, "resolved ipAddress:" + service.getInfo().getHostAddresses()[0]);
        }
    }

    public void serviceTypeAdded(ServiceEvent event) {
        // TODO Auto-generated method stub
        if (event.getType().contains(SERVICE_TYPE)) {
            mJmdns.addServiceListener(event.getType(), this);
        }
    }

    public void subTypeForServiceTypeAdded(ServiceEvent event) {
    }
}
