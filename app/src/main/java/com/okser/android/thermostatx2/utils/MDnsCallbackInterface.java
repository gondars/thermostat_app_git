package com.okser.android.thermostatx2.utils;


import com.okser.android.thermostatx2.model.Device;

public interface MDnsCallbackInterface {
    void onMDnsStarted();
    void onMDnsDeviceFound(Device device);
    void onMDnsStoped(boolean deviceFound);
}
