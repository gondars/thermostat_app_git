package com.okser.android.thermostatx2.model;

import android.app.Activity;
import android.content.Context;

import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.utils.MDnsCallbackInterface;
import com.okser.android.thermostatx2.utils.MDnsTask;
import com.okser.android.thermostatx2.utils.ThermostatUpdate;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LocationValue implements Serializable, MDnsCallbackInterface, ThermostatUpdate {

    private final Context mContext;
    private List<Device> mDevices;
    private Location mLocation;
    private List<ThermostatValue> mDeviceValue;
    private boolean delCheck;
    private MDnsTask mDnsTask;
    private boolean mMDNSRepeat;


    public LocationValue(Location location, final Context ctx) {
        mLocation = location;
        mDevices = location.getDevices();
        mContext = ctx;
        delCheck = false;
        mDeviceValue = new ArrayList<>();

        for(Device d : mDevices) {
            final ThermostatValue tstatData;
            tstatData = new ThermostatValue(d, this, mContext);
            mDeviceValue.add(tstatData);
        }
    }

    public boolean isDelCheck() {
        return delCheck;
    }

    public boolean setName(String name) {
        String n = mLocation.getName();
        if (!n.equals(name)) {
            mLocation.setName(name);
            return true;
        }
        return false;
    }

    public String getName() {
        return mLocation.getName();
    }

    public String geSSID() {
        return mLocation.getSsid();
    }

    public void toggleDelCheck() {
        delCheck = !delCheck;
    }

    @Override
    public void onMDnsStarted() {
    }

    @Override
    public void onMDnsStoped(boolean deviceFound) {
        if(deviceFound) {
            SnappyDBService.getInstance(mContext).updateLocation();
        }
        if (!mMDNSRepeat) {
            mDnsTask = null;
        }
    }

    @Override
    public void onMDnsDeviceFound(final Device newDevice) {
        onDeviceFound(newDevice);
    }

    @Override
    public void onDeviceFound(Device newDevice) {
        Device device = mLocation.addDevice(newDevice);
        final String ipAddress = device.getIPAddress();
        final String password = device.getMqttAuthToken();
        final String userName = device.getMqttApiKey();
        ThermostatValue tstatValue = getThermostatValue(ipAddress);
        // New device.
        if(tstatValue == null) {
            tstatValue = new ThermostatValue(device, this, mContext);
            mDeviceValue.add(tstatValue);
        }
        if(password == null || userName == null) {
            device.httpGetMqttConfig((Activity)mContext, new Device.MqttConfigReceivedListener() {
                @Override
                public void onReceived(boolean received) {
                    if(received) {
                        SnappyDBService.getInstance(mContext).updateLocation();
                    }
                }
            });
        }
        tstatValue.setConnectionType(ThermostatValue.ConnectionType.CONNECTION_HTTP);
        tstatValue.httpPostRTC();
    }

    public void MDNSScan(boolean repeat) {
        if (mDnsTask == null) {
            mDnsTask = new MDnsTask(mContext, this);
            mMDNSRepeat = repeat;
            mDnsTask.startScan(repeat);
        }
    }

    public void MDNSStop() {
        if (mDnsTask != null) {
            mDnsTask.stopScan();
            mMDNSRepeat = false;
            mDnsTask = null;
        }
    }

    public void onPause() {
        MDNSStop();
    }

    public int getCount() {
        return mLocation.getCount();
    }

    public ThermostatValue getThermostatValue(String ipAddress) {
        for(ThermostatValue t: mDeviceValue) {
            if(t.getIPaddress().equals(ipAddress)) {
                return t;
            }
        }
        return null;
    }

    public ThermostatValue getThermostatValue(int pos) {
        return mDeviceValue.get(pos);
    }

    public List<Device> getDevices() {
        return mDevices;
    }

    public List<ThermostatValue> getmDeviceValue() {
        return mDeviceValue;
    }

    public void onUpdate(final ThermostatValue.DataSetChangedListener listener) {
        for(Device d : mDevices) {
            final ThermostatValue deviceValue;
            final String ipAddress = d.getIPAddress();
            deviceValue = getThermostatValue(ipAddress);

            deviceValue.onUpdate(listener);
        }
    }
}
