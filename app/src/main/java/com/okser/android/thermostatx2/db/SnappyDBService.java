package com.okser.android.thermostatx2.db;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.model.Device;
import com.okser.android.thermostatx2.model.Location;
import com.okser.android.thermostatx2.model.LocationList;
import com.okser.android.thermostatx2.model.LocationValue;
import com.okser.android.thermostatx2.model.ThermostatValue;
import com.snappydb.DB;
import com.snappydb.DBFactory;
import com.snappydb.SnappydbException;

import java.util.ArrayList;
import java.util.List;


public class SnappyDBService  {
    private static final String DATABASE_KEY_LOCATIONS = "locations";
    private static final String TAG = SnappyDBService.class.getSimpleName();
    private static SnappyDBService sInstance;
    private LocationList mLocationList;
    private Location mLocation;
    private LocationValue mLocationValue;
    private List<LocationValue> mLocationValueList;
    private List<Device> mDevices;
    private DB mSnappyDB;
    private Context mContext;
    private String mCrtSSID;

    public String getmCrtSSID() {
        return mCrtSSID;
    }

    public void setmCrtSSID(String mCrtSSID) {
        this.mCrtSSID = mCrtSSID;
    }

    private SnappyDBService(Context context) {
        mContext = context;
        if(!open(context)) {
            init(context);
        }
    }

    public static synchronized void clear() {
        sInstance = null;
    }

    public static SnappyDBService getInstance(final Context context) {
        if (sInstance == null) {
            sInstance = new SnappyDBService(context);
        }
        return sInstance;
    }

    private boolean open(Context context) {
        try {
            mSnappyDB = DBFactory.open(context.getApplicationContext());
            if (mSnappyDB.exists(DATABASE_KEY_LOCATIONS)) {
                mLocationList = mSnappyDB.get(DATABASE_KEY_LOCATIONS, LocationList.class);
                List<Location> locationList = mLocationList.getData();
                mLocationValueList = new ArrayList<>();

                for(Location l : locationList) {
                    mLocationValueList.add(new LocationValue(l, context));
                }
                return true;
            } else {
                return false;
            }
        } catch (SnappydbException e) {
            Log.e(TAG, "initialize/open database failed: " + e.getMessage(), e);
            return false;
        }
    }

    public void init(Context context) {
        try {
            if (mSnappyDB == null) {
                mSnappyDB = DBFactory.open(context.getApplicationContext());
            }
            if (mSnappyDB.exists(DATABASE_KEY_LOCATIONS)) {
                mSnappyDB.del(DATABASE_KEY_LOCATIONS);
            }
            mLocationList = new LocationList();
            mLocationValueList = new ArrayList<>();
            //Location l = new Location(context.getString(R.string.default_location));
            Location l = new Location();
            addLocation(l);
        } catch (SnappydbException e) {
            Log.e(TAG, "destroy database failed: " + e.getMessage(), e);
        }
    }

    public void addLocation(Location location) {
        try {
            mLocationList.add(location);
            mLocationValueList.add(new LocationValue(location, mContext));
            if (mSnappyDB != null) {
                mSnappyDB.put(DATABASE_KEY_LOCATIONS, mLocationList);
            }
        } catch (Exception e) {
            Log.e(TAG, "add location failed (1): " + e.getMessage(), e);
        }
    }
/*
    public boolean isNewDevice(Device device) {
        for (Device i : mDevices) {
            if (i.getMACAddress().equals(device.getMACAddress())) {
                return false;
            }
        }
        return true;
    }

    public boolean addDevice(Device device) {
        if(isNewDevice(device)) {
            mDevices.add(device);
            return true;
        }
        return false;
    }

    public ThermostatValue getThermostatValue(Device device) {

    }
*/
    public int getCount() {
        return mLocationList.getData().size();
    }

    public Location getLocation(final int pos) {
        mLocation = mLocationList.getData().get(pos);
        setCurrentLocation(pos);
        return mLocation;
    }

    public void toggleDelCheck(int pos) {
        mLocationValueList.get(pos).toggleDelCheck();
    }

    public boolean isLocationChecked(int pos) {
        return mLocationValueList.get(pos).isDelCheck();
    }

    public boolean isDelChecked() {
        for(int l = mLocationValueList.size() - 1; l>=0; l--) {
            LocationValue lv = mLocationValueList.get(l);
            if(lv.isDelCheck()) {
                return true;
            }
        }
        return false;
    }
    public void delCheckedLocation() {

        for(int l = mLocationValueList.size() - 1; l>=0; l--) {
            LocationValue lv = mLocationValueList.get(l);
            if(lv.isDelCheck()) {
                mLocationValueList.remove(l);
                mLocationList.getData().remove(l);
            }
        }
        updateLocation();
    }

    public LocationValue getLocationValue(final int pos) {
        return  mLocationValueList.get(pos);
    }

    public ThermostatValue getTstatValue(final int loc_pos, final int tstat_pos) {
        return  mLocationValueList.get(loc_pos).getThermostatValue(tstat_pos);
    }

    public ThermostatValue getTstatValue(final Bundle args) {
        int loc_pos = args.getInt(Consts.KEY_LOCPOS);
        int tstat_pos = args.getInt(Consts.KEY_TSTATPOS);
        return getTstatValue(loc_pos, tstat_pos);
    }

    public void setCurrentLocation(final int pos) {
        mLocation = mLocationList.getData().get(pos);
        if(mLocationValueList == null) {
            mLocationValueList = new ArrayList<>();
            mLocationValueList.add(new LocationValue(mLocation, mContext));
        } else {
            mLocationValue = mLocationValueList.get(pos);
        }
        mDevices = mLocation.getDevices();
    }

    public List<Location> getLocationList() {
        return mLocationList.getData();
    }

    public synchronized void updateLocation() {
        try {
            if (mSnappyDB != null) {
                mSnappyDB.put(DATABASE_KEY_LOCATIONS, mLocationList);
            }
        } catch (Exception e) {
            Log.e(TAG, "update location failed (1): " + e.getMessage(), e);
        }
    }

}