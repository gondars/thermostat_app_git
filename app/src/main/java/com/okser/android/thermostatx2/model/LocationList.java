package com.okser.android.thermostatx2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LocationList implements Serializable {
    private final List<Location> mLocationList;

    public LocationList() {
        mLocationList = new ArrayList<>();
    }

    public void add(final Location location) {
        mLocationList.add(location);
    }

    public List<Location> getData() {
        return mLocationList;
    }
}
