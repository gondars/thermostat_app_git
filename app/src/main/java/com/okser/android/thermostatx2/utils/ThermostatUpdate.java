package com.okser.android.thermostatx2.utils;

import com.okser.android.thermostatx2.model.Device;

/**
 * Created by ding on 6/24/16.
 */
public interface ThermostatUpdate {
    void onDeviceFound(Device device);
}
