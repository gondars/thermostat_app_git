package com.okser.android.thermostatx2.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.consts.Fan;
import com.okser.android.thermostatx2.consts.System;
import com.okser.android.thermostatx2.consts.TemperatureUnit;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.ThermostatValue;
import com.okser.android.thermostatx2.utils.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThermostatAdapter extends BaseAdapter implements TextView.OnEditorActionListener {
    private final Context mCtx;
    private List<ThermostatValue> mDeviceValue;
    private final View.OnClickListener mOnClickListener;
    private ProgressDialog mProgressDialog;
    private AlertDialog mErrorDialog;
    private boolean mPauseUpdateOnEditing;

    public ThermostatAdapter(final List<ThermostatValue> deviceValue, final Context ctx, View.OnClickListener onClickListener) {
        super();
        mDeviceValue = deviceValue;
        mOnClickListener = onClickListener;
        mCtx = ctx;
        mProgressDialog = null;
        mPauseUpdateOnEditing = false;
    }

    private void showErrorDialog() {
        if (mErrorDialog != null) {
            mErrorDialog.dismiss();
            mErrorDialog = null;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
        builder.setMessage(R.string.msg_cannotfetchdata);
        builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                //fetchAllData();
            }
        });
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                notifyDataSetChanged();
            }
        });
        mErrorDialog = builder.create();
        mErrorDialog.show();
    }


    public void destory() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        if (mErrorDialog != null) {
            mErrorDialog.dismiss();
            mErrorDialog = null;
        }
    }

    @Override
    public int getCount() {
        return mDeviceValue.size();
        /*
        int n = mLocationValue.getCount();
        return n;
        */
    }

    @Override
    public ThermostatValue getItem(final int position) {
        return mDeviceValue.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ThermostatValue  value = mDeviceValue.get(position);
        View v;

        if (convertView == null) {
            v = View.inflate(parent.getContext(), R.layout.item_thermostat, null);
        } else {
            v = convertView;
        }

        final EditText main = (EditText) v.findViewById(R.id.main);


        TextView temperature = (TextView) v.findViewById(R.id.temperature);
        TextView fan = (TextView) v.findViewById(R.id.label_fan);
        TextView humidity = (TextView) v.findViewById(R.id.label_humidity);
        TextView system = (TextView) v.findViewById(R.id.label_system);
        RelativeLayout next = (RelativeLayout) v.findViewById(R.id.device);
        RelativeLayout middleBox = (RelativeLayout) v.findViewById(R.id.second);

        double deviceWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
        int middleBoxWidth =  middleBox.getLayoutParams().width;
        int margins =  ((int)deviceWidth - 20 -  middleBoxWidth*3) / 2;

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) middleBox.getLayoutParams();
        params.setMargins(margins, 0, margins, 0);
        middleBox.setLayoutParams(params);

        TextView sys = (TextView) v.findViewById(R.id.sys);
        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) sys.getLayoutParams();
        params2.setMargins(margins, 0, margins, 0);
        sys.setLayoutParams(params2);

        //ImageView info = (ImageView) v.findViewById(R.id.ic_info);
        //info.setTag(position);

        //info.setOnClickListener(mOnClickListener);
        View devicepanel = v.findViewById(R.id.device);
        temperature.setOnClickListener(mOnClickListener);
        temperature.setTag(position);
        devicepanel.setOnClickListener(mOnClickListener);
        devicepanel.setTag(position);
        main.setTag(position);
        main.setOnEditorActionListener(this);

        if (convertView == null) {
            String name = value.getName();
            if (TextUtils.isEmpty(name)) {
                main.setText(mCtx.getString(R.string.default_devicename));
            } else {
                main.setText(name);
            }
        }

        if (value.getConnectionType() == ThermostatValue.ConnectionType.CONNECTION_NONE) {
            fan.setText("");
            system.setText("");
            humidity.setText("");
            String name = value.getName() + " NA";
            main.setText(name);
            Utils.setTemperature(v.getContext(), temperature, 0, TemperatureUnit.F);
            return v;
        }

        final Fan fanValue = value.getFan();
        if (fanValue == Fan.On) {
            fan.setText(R.string.fan_status_on);
        } else if (fanValue == Fan.Circulate) {
            fan.setText(R.string.fan_status_circulate);
        } else {
            fan.setText(R.string.fan_status_auto);
        }

        final System systemValue = value.getSystem();
        if (systemValue == System.Auto) {
            system.setText(R.string.system_autochangeover);
        } else if (systemValue == System.Cool) {
            system.setText(R.string.system_cool);
        } else if (systemValue == System.Heat) {
            system.setText(R.string.system_heat);
        } else {
            system.setText(R.string.system_off);
        }
        String h = value.getHumidity() + mCtx.getString(R.string.humidity_unit);
        humidity.setText(h);
        Utils.setTemperature(v.getContext(), temperature, value.getTemperature(), TemperatureUnit.F);
        return v;
    }

    @Override
    public int getItemViewType(int position) {
        int n = super.getItemViewType(position);
        return n;
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(actionId == EditorInfo.IME_ACTION_DONE) {
            final String s = v.getText().toString();
            final Integer position = (Integer) v.getTag();
            final ThermostatValue  value = mDeviceValue.get(position);
            if (!s.equals(value.getName())) {
                value.setName(s);
                SnappyDBService.getInstance(mCtx).updateLocation();
                final Map<String, String> params = new HashMap<>();
                params.put(Consts.KEY_THERMOSTAT_NAME, s);
                value.PostRequest(params, null);
            }
            mPauseUpdateOnEditing = false;
            Utils.hideKeyboard(v.getContext(), v);
            v.clearFocus();
            return true;
        }
        return false;
    }



    /*
    public void onUpdate(ThermostatValue.DataSetChangedListener listener) {
        if(mPauseUpdateOnEditing) {
            return;
        }
        for(ThermostatValue t: mDeviceValue) {
            t.onUpdate(listener);
        }
    }
    */
}
