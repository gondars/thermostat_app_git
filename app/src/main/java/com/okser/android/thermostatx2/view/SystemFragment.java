package com.okser.android.thermostatx2.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.consts.Fan;
import com.okser.android.thermostatx2.consts.System;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.ThermostatValue;
import com.okser.android.thermostatx2.utils.ITabListener;

import java.util.HashMap;
import java.util.Map;

public class SystemFragment extends Fragment implements ITabListener, RadioGroup.OnCheckedChangeListener, DialogInterface.OnClickListener {
    private Fan mFan;
    private RadioGroup mFanGroup;
    private Fan mOldFan;
    private System mOldSystem;
    private System mSystem;
    private RadioGroup mSystemGroup;
    private ThermostatValue mTstatValue;

    public static SystemFragment newInstance(Bundle args) {
        // Bundle args = new Bundle(extras);
        SystemFragment f = new SystemFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public boolean doBackAction() {
        return true;
    }

    public void doSave(final ThermostatValue.DataSetChangedListener listener) {
        if(mTstatValue == null) {
            return;
        }
        /*
        mTstatValue.setSystem(mSystem);
        mTstatValue.setFan(mFan);

        mOldSystem = mSystem;
        mOldFan = mFan;
        updateView();
        */
        final Map<String, String> params = new HashMap<>();
        params.put(Consts.KEY_SYSTEM, String.valueOf(mSystem.ordinal()));
        params.put(Consts.KEY_FAN, String.valueOf(mFan.ordinal()));
        mTstatValue.PostRequest(params, listener);
        /*
        ConferenceFragment f = (ConferenceFragment) getParentFragment();
        f.afterSave();
        */
    }

    @Override
    public void onCheckedChanged(final RadioGroup group, final int checkedId) {
        if (group.getId() == R.id.radio_systm) {
            if (checkedId == R.id.system_auto) {
                mSystem = System.Auto;
            } else if (checkedId == R.id.system_heat) {
                mSystem = System.Heat;
            } else if (checkedId == R.id.system_cool) {
                mSystem = System.Cool;
            } else {
                mSystem = System.Off;
            }
        } else if (group.getId() == R.id.radio_fan) {
            if (checkedId == R.id.fan_auto) {
                mFan = Fan.Auto;
            } else if (checkedId == R.id.fan_circulate) {
                mFan = Fan.Circulate;
            } else {
                mFan = Fan.On;
            }
        }
        updateView();
    }

    private void updateView() {
        boolean changed = ((mFan != mOldFan) || (mSystem != mOldSystem));
        Fragment f = getParentFragment();
        if (f instanceof ConferenceFragment) {
            ((ConferenceFragment) f).updateRightBtn(changed);
        }
    }

    @Override
    public void onClick(final DialogInterface dialog, final int which) {
       doSave(null);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_system, container, false);
        mSystemGroup = (RadioGroup) view.findViewById(R.id.radio_systm);
        mFanGroup = (RadioGroup) view.findViewById(R.id.radio_fan);
        mSystemGroup.setOnCheckedChangeListener(this);
        mFanGroup.setOnCheckedChangeListener(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Bundle args = new Bundle(getArguments());
        mTstatValue = SnappyDBService.getInstance(getActivity()).getTstatValue(args);
        if(mTstatValue != null) {
            onUpdate(mTstatValue);
        }
    }

    public void onUpdate(ThermostatValue tstatValue) {
        mFan  = tstatValue.getFan();
        mSystem = tstatValue.getSystem();
        mOldFan = mFan;
        mOldSystem = mSystem;
        bindView(getView());
    }

    private void bindView(View view) {
        if (mSystem == System.Auto) {
            mSystemGroup.check(R.id.system_auto);
        } else if (mSystem == System.Off) {
            mSystemGroup.check(R.id.system_off);
        } else if (mSystem == System.Cool) {
            mSystemGroup.check(R.id.system_cool);
        } else {
            mSystemGroup.check(R.id.system_heat);
        }
        if (mFan == Fan.Auto) {
            mFanGroup.check(R.id.fan_auto);
        } else if (mFan == Fan.On) {
            mFanGroup.check(R.id.fan_on);
        } else if (mFan == Fan.Circulate) {
            mFanGroup.check(R.id.fan_circulate);
        }
        updateView();
    }
}
