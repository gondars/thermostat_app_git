package com.okser.android.thermostatx2.model;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.consts.Fan;
import com.okser.android.thermostatx2.consts.OperationMode;
import com.okser.android.thermostatx2.consts.ProgramType;
import com.okser.android.thermostatx2.consts.System;
import com.okser.android.thermostatx2.consts.TemperatureUnit;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.mqtt_iot.IoTActionListener;
import com.okser.android.thermostatx2.mqtt_iot.IoTCallbacks;
import com.okser.android.thermostatx2.mqtt_iot.ThermostatMqttClient;
import com.okser.android.thermostatx2.utils.ExStringRequest;
import com.okser.android.thermostatx2.utils.NetworkUtil;
import com.okser.android.thermostatx2.view.MainActivity;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class ThermostatValue implements Serializable, IoTCallbacks, IoTActionListener {
    public enum ConnectionType {
        CONNECTION_NONE,
        CONNECTION_HTTP,
        CONNECTION_MQTT
    }

    public enum MqttState {
        IDLE,
        CONNECTED,
        SUBSCRIBED,
    }

    private String mGetTag = "GetTag";
    private LocationValue mlocValue;
    private Device mDevice;
    private String mIPaddress;
    private String mName;
    private final Context mCtx;
    private final RequestQueue mRequestQueue;
    private boolean isConfigRead;

    // status
    private boolean ismStatusUpdating;
    private int mGetStatusFailCount;
    private Fan mFan;
    private int mHumidity;
    private System mSystem;
    private int mTemperature;
    private int mTargetTemperatureCool;
    private int mTargetTemperatureHeat;
    private boolean mConfigUpdate;
    private boolean mHoldTimeEnable;
    private int mConfigCheckSum;
    private int mHoldFor;
    private  ConnectionType connectionType;

    // Config
    private boolean isConfigUpdating;
    private OperationMode mOperationMode;
    private int mPermanentCool;
    private int mPermanentHeat;
    private int mTemporaryCool;
    private int mTemporaryHeat;
    private int mCoolLimit;
    private int mHeatLimit;
    private TemperatureUnit mTemperatureUnit;
    private ProgramType mProgramType;
    private String mSchedule1;
    private String mSchedule2;
    // private IoTClient mIoTClient;
    private ThermostatMqttClient mIoTClient;
    private MqttState mqttState;
    private String MqttSubTopic;
    private String MqttPubGetTopic;
    private String MqttPubSetTopic;
    private DataSetChangedListener dataSetChangedListener;

    public interface DataSetChangedListener {
        enum ChangedType {
            STATUS,
            GET_CONFIG,
            POST_CONFIG,
            RTC,
            NONE
        }
        void DataSetChanged(ChangedType c, boolean configUpdate);
    }

    public interface PostDataComplete {
        void onPostDataComplete(boolean success);
    }

    public ThermostatValue(Device device, LocationValue locValue, final Context ctx) {
        mDevice = device;
        mIPaddress = device.getIPAddress();
        mName = device.getName();
        mlocValue = locValue;
        mCtx = ctx;
        isConfigRead = false;
        mGetStatusFailCount = 0;
        connectionType = ConnectionType.CONNECTION_NONE;
        mRequestQueue = ((MainActivity)ctx).mRequestQueue;
    }

    public String getIPaddress() {
        return mIPaddress;
    }

    public String getMACAddr() {
        return mDevice.getMACAddress();
    }
    public String getWiFiName() {
        return mDevice.getSsid();
    }

    public String getName() {
        return mDevice.getName();
    }

    public void setName(String name) {
        this.mName = name;
        mDevice.setName(name);
    }

    public void setIPaddress(String host) {
        this.mIPaddress = host;
    }

    public Fan getFan() {
        return mFan;
    }

    public void setFan(final Fan fan) {
        mFan = fan;
    }

    public int getHumidity() {
        return mHumidity;
    }

    public void setHumidity(final int humidity) {
        mHumidity = humidity;
    }

    public System getSystem() {
        return mSystem;
    }

    public void setSystem(final System system) {
        mSystem = system;
    }

    public int getTemperature() {
        return mTemperature;
    }

    public void setTemperature(final int temperature) {
        mTemperature = temperature;
    }

    public int getmTargetTemperatureCool() {
        return mTargetTemperatureCool;
    }

    public int getmTargetTemperatureHeat() {
        return mTargetTemperatureHeat;
    }

    public boolean getConfigUpdate() { return mConfigUpdate || !isConfigRead; }

    public void setConfigUpdate(final boolean configUpdate) {mConfigUpdate = configUpdate;}

    public boolean isHoldTimeEnable() {
        return mHoldTimeEnable;
    }

    public int getHoldFor() {
        return mHoldFor;
    }

    public void setHoldFor(int HoldFor) {
        this.mHoldFor = HoldFor;
    }

    public OperationMode getOperationMode() {
        return mOperationMode;
    }

    public void setOperationMode(OperationMode operationMode) {
        this.mOperationMode = operationMode;
    }

    public int getPermanentCool() {
        return mPermanentCool;
    }

    public void setPermanentCool(int permanentCool) {
        this.mPermanentCool = permanentCool;
    }

    public int getPermanentHeat() {
        return mPermanentHeat;
    }

    public void setPermanentHeat(int permanentHeat) {
        this.mPermanentHeat = permanentHeat;
    }

    public int getTemporaryCool() {
        return mTemporaryCool;
    }

    public void setTemperaryCool(int temporaryCool) {
        this.mTemporaryCool = temporaryCool;
    }

    public int getTemporaryHeat() {
        return mTemporaryHeat;
    }

    public int getCoolLimit() {
        return mCoolLimit;
    }

    public int getHeatLimit() {
        return mHeatLimit;
    }

    public void setTemporaryHeat(int temporaryHeat) {
        this.mTemporaryHeat = temporaryHeat;
    }

    public TemperatureUnit getTemperatureUnit() {
        return mTemperatureUnit;
    }

    public void setTemperatureUnit(TemperatureUnit temperatureUnit) {
        this.mTemperatureUnit = temperatureUnit;
    }

    public ProgramType getProgramType() {
        return mProgramType;
    }

    public void setProgramType(ProgramType programType) {
        this.mProgramType = programType;
    }

    public String getSchedule1() {
        return mSchedule1;
    }

    public void setSchedule1(String schedule1) {
        this.mSchedule1 = schedule1;
    }

    public String getSchedule2() {
        return mSchedule2;
    }

    public void setSchedule2(String schedule2) {
        this.mSchedule2 = schedule2;
    }

    public ConnectionType getConnectionType() {
        return connectionType;
    }

    public void setConnectionType(ConnectionType connectionType) {
        this.connectionType = connectionType;
    }

    @Override
    public String toString() {
        return "ThermostatValue{" +
                "mFan=" + mFan +
                ", mHumidity=" + mHumidity +
                ", mSystem=" + mSystem +
                ", mTemperature=" + mTemperature +
                '}';
    }

    private void parseConfig(String response) {
        //TODO fix parseInt
        boolean saveDB = false;
        String[] datapair = response.split(",");
        for (String strValue : datapair) {
            if (TextUtils.isEmpty(strValue)) {
                continue;
            }

            String[] keys = strValue.split(":");
            if (keys.length >= 2) {
                if (TextUtils.isEmpty(keys[0]) || keys[1] == null) {
                    continue;
                }
                final String key = keys[0].trim();
                final String value = keys[1].trim();
                if (Consts.KEY_THERMOSTAT_NAME.equals(key)) {
                    if (!value.equals(mName)) {
                        setName(value);
                        saveDB = true;
                    }
                } else if (Consts.KEY_LOCATION_NAME.equals(key)) {
                    saveDB = mlocValue.setName(value);;
                } else if (Consts.KEY_SCHEDULE_1.equals(key)) {
                    mSchedule1 = value;
                } else if (Consts.KEY_SCHEDULE_2.equals(key)) {
                    mSchedule2 = value;
                } else if (Consts.KEY_RTC.equals(key)) {

                } else {
                    try {
                        int num = TextUtils.isEmpty(value) ? 0 : Integer.parseInt(value);
                        if (Consts.KEY_TEMPORAY_COOL.equals(key)) {
                            mTemporaryCool = num;
                        } else if (Consts.KEY_TEMPORAY_HEAT.equals(key)) {
                            mTemporaryHeat = num;
                        } else if (Consts.KEY_PERMANENT_COOL.equals(key)) {
                            mPermanentCool = num;
                        } else if (Consts.KEY_PERMANENT_HEAT.equals(key)) {
                            mPermanentHeat = num;
                        } else if (Consts.KEY_COOL_LIMIT.equals(key)) {
                            mCoolLimit = num;
                        } else if (Consts.KEY_HEAT_LIMIT.equals(key)) {
                            mHeatLimit = num;
                        } else if (Consts.KEY_MODE.equals(key)) {
                            mOperationMode = OperationMode.get(num);
                        } else if (Consts.KEY_FAN.equals(key)) {
                            mFan = Fan.get(num);
                        } else if (Consts.KEY_SYSTEM.equals(key)) {
                            mSystem = System.get(num);
                        } else if (Consts.KEY_PROGRAM_TYPE.equals(key)) {
                            mProgramType = ProgramType.get(num);
                        } else if (Consts.KEY_TEMPERATURE_UNIT.equals(key)) {
                            mTemperatureUnit = TemperatureUnit.get(num);
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (saveDB) {
            SnappyDBService.getInstance(mCtx).updateLocation();
        }
        isConfigRead = true;
        mConfigUpdate = false;
    }

    private void parseStatus(String response) {
        String[] datapair = response.split(",");
        for (String strValue : datapair) {
            if (TextUtils.isEmpty(strValue)) {
                continue;
            }
            String[] keys = strValue.split(":");
            if (keys.length >= 2) {
                if (TextUtils.isEmpty(keys[0]) || keys[1] == null) {
                    continue;
                }
                final String key = keys[0].trim();
                final String value = keys[1].trim();
                final int num = TextUtils.isEmpty(value) ? 0 : Integer.parseInt(value);

                if (Consts.KEY_STATUS_HUMIDITY.equals(key)) {
                    mHumidity = num;
                } else if (Consts.KEY_STATUS_TEMPERATURE.equals(key)) {
                    mTemperature = num;
                } else if (Consts.KEY_STATUS_TARGET_TEMPERATURE_COOL.equals(key)) {
                    mTargetTemperatureCool = num;
                } else if (Consts.KEY_STATUS_TARGET_TEMPERATURE_HEAT.equals(key)) {
                    mTargetTemperatureHeat = num;
                } else if (Consts.KEY_STATUS_CONFIG_UPDATE.equals(key)) {
                    if(mConfigCheckSum == 0 && isConfigRead) {
                        mConfigUpdate = false;
                        mConfigCheckSum = num;
                    } else if (num != mConfigCheckSum) {
                        mConfigUpdate = true;
                        mConfigCheckSum = num;
                        /*
                    } else {
                        mConfigUpdate = false;
                    */
                    }
                } else if (Consts.KEY_HOLDFOR.equals(key)) {
                    mHoldFor = num;
                    if(mHoldFor >0) {
                        if(mHoldFor > 1440) {
                            mHoldFor -= 1440;
                        }
                        mHoldTimeEnable = true;
                    } else {
                        mHoldTimeEnable = false;
                    }
                }
            }
        }
    }

    protected void httpGetStatus(final DataSetChangedListener listener) {
        if(ismStatusUpdating) {
            return;
        }
        final String apiStatus = String.format(Consts.KEY_API_STATUS, mIPaddress);
        mRequestQueue.getCache().clear();
        // mRequestQueue.cancelAll(mGetTag);
        ExStringRequest status = new ExStringRequest(apiStatus, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                connectionType = ConnectionType.CONNECTION_HTTP;
                Log.i(ThermostatValue.class.getSimpleName(), "GET response:" + response + " host:" + mIPaddress);
                parseStatus(response);
                listener.DataSetChanged(DataSetChangedListener.ChangedType.STATUS, getConfigUpdate());
                mGetStatusFailCount = 0;
                ismStatusUpdating = false;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                mGetStatusFailCount++;
                if(mGetStatusFailCount >= 2) {
                    mGetStatusFailCount = 0;
                    connectionType = ConnectionType.CONNECTION_MQTT;
                }
                listener.DataSetChanged(DataSetChangedListener.ChangedType.NONE, false);
                Log.e(ThermostatValue.class.getSimpleName(), "response: error" + error.getMessage(), error);
                ismStatusUpdating = false;
                Toast.makeText(mCtx, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        status.setShouldCache(false);
        status.setTag(mGetTag);
        status.setRetryPolicy(new DefaultRetryPolicy(
                1500, // MY_SOCKET_TIMEOUT_MS,
                2, //DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.cancelAll(mGetTag);
        mRequestQueue.add(status);
        ismStatusUpdating = true;

    }

    public void httpGetConfig(final DataSetChangedListener listener) {
        if(isConfigUpdating) {
            return;
        }
        final String apiConfig = String.format(Consts.KEY_API_CFG, mIPaddress);
        ExStringRequest config = new ExStringRequest(apiConfig, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.i(ThermostatValue.class.getSimpleName(), "GET response:" + response + " host:" + mIPaddress);
                parseConfig(response);
                if(listener != null) {
                    listener.DataSetChanged(DataSetChangedListener.ChangedType.GET_CONFIG, false);
                }
                isConfigUpdating = false;
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if(listener != null) {
                    listener.DataSetChanged(DataSetChangedListener.ChangedType.NONE, false);
                }
                Log.e(ThermostatValue.class.getSimpleName(), "response: error" + error.getMessage(), error);
                Toast.makeText(mCtx, error.getMessage(), Toast.LENGTH_LONG).show();
                isConfigUpdating = false;
            }
        });
        config.setShouldCache(false);
        config.setRetryPolicy(new DefaultRetryPolicy(
                1500, //MY_SOCKET_TIMEOUT_MS,
                2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        mRequestQueue.add(config);
        config.setTag(mGetTag);
        isConfigUpdating = true;
        Log.i(ThermostatValue.class.getSimpleName(), "Sequence: "+config.getSequence() + " " + apiConfig);
    }

    public void httpPostRTC() {
        final Map<String, String> params = new HashMap<>();
        Date now = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy MM dd HH mm ss", Locale.US);
        String rtc = "RTC " + format.format(now);
        params.put(Consts.KEY_RTC, rtc);
        final String url = String.format(Consts.KEY_API_UPDATE, mIPaddress);
        PostRequest request = new PostRequest(params, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                httpGetConfig(null);
                Log.i(ThermostatValue.class.getSimpleName(), "Post response:" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Log.e(this.getClass().getSimpleName(), "onErrorResponse:" + error.getMessage(), error);
            }
        });
        request.setShouldCache(false);
        mRequestQueue.add(request);
    }

    protected void httpPostConfig(Map<String, String> params, final DataSetChangedListener listener) {
        final String url = String.format(Consts.KEY_API_UPDATE, mIPaddress);
        //mRequestQueue.cancelAll(mGetTag);
        PostRequest request = new PostRequest(params, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                if(listener != null) {
                    listener.DataSetChanged(DataSetChangedListener.ChangedType.POST_CONFIG, false);
                }
                Log.i(ThermostatValue.class.getSimpleName(), "Post response:" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                if(listener != null) {
                    listener.DataSetChanged(DataSetChangedListener.ChangedType.NONE, false);
                }
                Log.e(this.getClass().getSimpleName(), "onErrorResponse:" + error.getMessage(), error);
            }
        });
        final Map<String, String> sch = new HashMap<>();
        sch.put(Consts.CMD_SCH, Consts.CMD_SCH);
        request.append(sch);
        request.setRetryPolicy(new DefaultRetryPolicy(
                1500, //MY_SOCKET_TIMEOUT_MS,
                2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        request.setShouldCache(false);
        mRequestQueue.add(request);
    }

    private class PostRequest extends StringRequest {
        StringBuilder encodedParams = new StringBuilder();
        String paramsEncoding = getParamsEncoding();

        public PostRequest(Map<String, String> params, final String url, final Response.Listener<String> listener, final Response.ErrorListener errorListener) {
            super(Method.POST, url, listener, errorListener);
            append(params);
        }

        public void append(Map<String, String> params) {
            try {
                for (Map.Entry<String, String> entry : params.entrySet()) {
                    encodedParams.append(URLEncoder.encode(entry.getKey(), paramsEncoding));
                    encodedParams.append('=');
                    encodedParams.append(URLEncoder.encode(entry.getValue(), paramsEncoding));
                    encodedParams.append('&');
                }
            } catch (UnsupportedEncodingException uee) {
                uee.printStackTrace();
                throw new RuntimeException("Encoding not supported: " + paramsEncoding, uee);
            }
        }

        @Override
        public byte[] getBody() throws AuthFailureError {
            try {
                String body = encodedParams.toString();
                Log.i(this.getClass().getSimpleName(), "Post body:" + body);
                return body.getBytes(paramsEncoding);
            } catch (UnsupportedEncodingException uee) {
                throw new RuntimeException("Encoding not supported: " + paramsEncoding, uee);
            }
        }

        @Override
        protected Response<String> parseNetworkResponse(NetworkResponse response) {
            Log.i(this.getClass().getSimpleName(), "Post response:" + response.headers + " code:" + response.statusCode);
            return Response.success("", HttpHeaderParser.parseCacheHeaders(response));
        }
    }

    /**
     * Execute onStart after MQTT Config is received.
     */

    public void onMqttStart() {

        mqttState = MqttState.IDLE;
        String deviceID = mDevice.getMqttDeviceID();
        String devicetype = mDevice.getMqttDeviceType();

        if(mIoTClient == null) {
            try {
                mIoTClient = new ThermostatMqttClient(mCtx, mDevice, true, null);
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
        MqttSubTopic = "iot-2/type/" + devicetype + "/id/"+ deviceID + "/evt/+/fmt/json";
        MqttPubGetTopic = "iot-2/type/" + devicetype + "/id/"+ deviceID + "/cmd/get/fmt/string";
        MqttPubSetTopic = "iot-2/type/" + devicetype + "/id/"+ deviceID + "/cmd/set/fmt/string";
    }

    public void MqttGetStatus(final DataSetChangedListener listener) {
        dataSetChangedListener = listener;
        try {
            mIoTClient.publish(MqttPubGetTopic, "status", 0, false, this);
        } catch (MqttException e) {

        }
    }

    public  void MqttGetConfig() {
        try {
            mIoTClient.publish(MqttPubGetTopic, "config", 0, false, this);
        } catch (MqttException e) {

        }
    }

    public void MqttPostConfig(Map<String, String> params, final DataSetChangedListener listener) {
        dataSetChangedListener = listener;
        StringBuilder encodedParams = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            encodedParams.append(entry.getKey());
            encodedParams.append('=');
            encodedParams.append(entry.getValue());
            encodedParams.append('&');
        }
        encodedParams.append(Consts.CMD_SCH);
        encodedParams.append('=');
        encodedParams.append(Consts.CMD_SCH);
        encodedParams.append('&');

        try {
            mIoTClient.publish(MqttPubSetTopic, encodedParams.toString(), 0, true, this);
        } catch (MqttException e) {

        }
    }

    public void onUpdate(final DataSetChangedListener listener) {
        switch(connectionType) {
            case CONNECTION_HTTP:
                if(getConfigUpdate()) {
                    httpGetConfig(listener);
                } else {
                    httpGetStatus(listener);
                }
                mlocValue.MDNSStop();
                break;
            case CONNECTION_NONE:
                if(NetworkUtil.getConnectionStatus(mCtx) == NetworkUtil.NOT_CONNECTED) {
                    break;
                }
                // Try MQTT connection.
                connectionType = ConnectionType.CONNECTION_MQTT;
            case CONNECTION_MQTT:
                try {
                    if(mIoTClient == null) {
                        onMqttStart();
                    }
                    switch(mqttState) {
                        case IDLE:
                            mIoTClient.connect(this, this);
                            break;
                        case CONNECTED:
                            break;
                        case SUBSCRIBED:
                            MqttGetStatus(listener);
                            mlocValue.MDNSScan(true);
                            break;
                    }

                } catch (MqttException e) {
                    e.printStackTrace();
                /*
                if (e.getReasonCode() == (Constants.ERROR_BROKER_UNAVAILABLE)) {
                    // error while connecting to the broker - send an intent to inform the user
                    Intent actionIntent = new Intent(Constants.ACTION_INTENT_CONNECTIVITY_MESSAGE_RECEIVED);
                    actionIntent.putExtra(Constants.CONNECTIVITY_MESSAGE, Constants.ERROR_BROKER_UNAVAILABLE);
                    context.sendBroadcast(actionIntent);
                }
                */
                }
                break;
        }
    }

    public void PostRequest(Map<String, String> params, final DataSetChangedListener listener) {
        switch(connectionType) {
            case CONNECTION_NONE:
                return;
            case CONNECTION_HTTP:
                httpPostConfig(params, listener);
                break;
            case CONNECTION_MQTT:
                MqttPostConfig(params, listener);
                break;
        }
    }

    // IOT actionListener.

    @Override
    public void onSuccess(IMqttToken asyncActionToken) {
        try {
            String action = (String) asyncActionToken.getUserContext();
            switch(mqttState) {
                case IDLE:
                    if(action.startsWith("Connect")) {
                        mqttState = MqttState.CONNECTED;
                        mIoTClient.subscribe(MqttSubTopic, 0, this);
                    }
                    break;
                case CONNECTED:
                    if(action.startsWith("Subscribe")) {
                        mqttState = MqttState.SUBSCRIBED;
                    }
                    break;
                case SUBSCRIBED:
                    break;
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
        // mIoTClient.setAction(IoTClient.Action.NONE);
    }

    // IOT callback.

    @Override
    public void connectionLost(Throwable cause) {
        mqttState       = MqttState.IDLE;
        connectionType  = ConnectionType.CONNECTION_NONE;
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
/*
        //String runningActivity = app.getCurrentRunningActivity();
        //if (runningActivity != null && runningActivity.equals(IoTPagerFragment.class.getName())) {
        Intent actionIntent = new Intent(Constants.APP_ID + Constants.INTENT_IOT);
        actionIntent.putExtra(Constants.INTENT_DATA, Constants.INTENT_DATA_RECEIVED);
        context.sendBroadcast(actionIntent);
        //}
*/
        String payload = new String(message.getPayload());
        String[] data = payload.split("=");
        if(data[0].contains("status")) {
            String status = data[1].replaceAll("[\r\n\"{}; ]", "");
            parseStatus(status);
            if(getConfigUpdate()) {
                MqttGetConfig();
            } else {
                ((Activity)mCtx).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(dataSetChangedListener != null) {
                            dataSetChangedListener.DataSetChanged(DataSetChangedListener.ChangedType.STATUS, false);
                        }
                    }
                });
            }
        } else if(data[0].contains("cfg")) {
            String config = data[1].replaceAll("[\r\n\"{};]", "");
            parseConfig(config);
            ((Activity)mCtx).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(dataSetChangedListener != null) {
                        dataSetChangedListener.DataSetChanged(DataSetChangedListener.ChangedType.GET_CONFIG, false);
                    }
                }
            });
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }
}
