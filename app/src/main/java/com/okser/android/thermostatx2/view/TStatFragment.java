package com.okser.android.thermostatx2.view;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.TimePicker;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.consts.OperationMode;
import com.okser.android.thermostatx2.consts.TemperatureUnit;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.ThermostatValue;
import com.okser.android.thermostatx2.utils.ITabListener;
import com.okser.android.thermostatx2.utils.Utils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class TStatFragment extends Fragment implements ITabListener, View.OnClickListener, TimePickerDialog.OnTimeSetListener {
    private TextView mCoolText;
    private TextView mHeatText;
    private TextView mLabelHold;
    private CheckBox mModeSwitch;
    private TextView mModeTemperatureDesp;
    private TextView mModeValue;
    View coolup;
    View cooldown;
    View heatup;
    View heatdown;
    private int mOldHoldFor;
    private OperationMode mOldMode;
    private int mOldPermanentCool;
    private int mOldPermanentHeat;
    private int mOldTemporaryCool;
    private int mOldTemporaryHeat;
    private int mCoolLimit;
    private int mHeatLimit;
    private boolean mOldHoldTimeEnable;
    private int mOldSetHour;
    private int mOldSetMin;

    private OperationMode mOperationMode;
    private int mPermanentCool;
    private int mPermanentHeat;
    private int mTemporaryCool;
    private int mTemporaryHeat;
    private int mTargetTemperatureCool;
    private int mTargetTemperatureHeat;
    private TemperatureUnit mTemperatureUnit;
    private int mHoldFor;
    private boolean mHoldTimeEnable;
    //private boolean mTimeSet;
    private int mSetHour;
    private int mSetMin;
    private ThermostatValue mTstatValue;


    public static TStatFragment newInstance(Bundle args) {
        // Bundle args = new Bundle(extras);
        TStatFragment f = new TStatFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public boolean doBackAction() {
        return true;
    }

    public void doSave(final ThermostatValue.DataSetChangedListener listener) {
        int holdfor;
        if(mTstatValue == null) {
            return;
        }


        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minu = now.get(Calendar.MINUTE);
        if (mHoldTimeEnable) {
            holdfor = (mSetHour* 60 + mSetMin) - (hour * 60 + minu) ;
        } else {
            holdfor = 0;
        }
        if(holdfor > 1) {
            mHoldFor = holdfor;
        } else if(holdfor < -10) {
            mHoldFor = 1440 + holdfor;
        } else {
            mHoldFor = 0;
        }
        Log.i(this.getClass().getSimpleName(), "HOS:" + mHoldFor);

        final Map<String, String> params = new HashMap<>();

        params.put(Consts.KEY_SETHOLDTIME, String.valueOf(mHoldFor));
        if (mOperationMode == OperationMode.Hold) {
            params.put(Consts.KEY_PERMANENT_HEAT, String.valueOf(mPermanentHeat));
            params.put(Consts.KEY_PERMANENT_COOL, String.valueOf(mPermanentCool));
        } else {
            params.put(Consts.KEY_TEMPORAY_HEAT, String.valueOf(mTemporaryHeat));
            params.put(Consts.KEY_TEMPORAY_COOL, String.valueOf(mTemporaryCool));
        }

        if (mHoldTimeEnable) {
            params.put(Consts.KEY_HOLDTIMEENABLE, "1");
        } else {
            params.put(Consts.KEY_HOLDTIMEENABLE, "0");
        }
        params.put(Consts.KEY_MODE, String.valueOf(mOperationMode.ordinal()));
        mTstatValue.PostRequest(params, listener);

    }

    private void updateView() {

        boolean coldDownEnabled = mTargetTemperatureCool > Consts.TEMP_MIN &&
                (mTargetTemperatureCool - 1) > mTargetTemperatureHeat &&
                mTargetTemperatureCool > mCoolLimit;

        boolean coldUpEnabled = mTargetTemperatureCool < Consts.TEMP_MAX;

        boolean heatDownEnabled = mTargetTemperatureHeat > Consts.TEMP_MIN;

        boolean heatUpEnabled = mTargetTemperatureHeat < Consts.TEMP_MAX &&
                (mTargetTemperatureCool - 1) > mTargetTemperatureHeat &&
                mTargetTemperatureHeat < mHeatLimit;

        if (mOperationMode == OperationMode.Schedule && !mHoldTimeEnable) {
            coldDownEnabled = false;
            coldUpEnabled = false;
            heatDownEnabled = false;
            heatUpEnabled = false;
        } else
            switch (mTstatValue.getSystem()) {
                case Off:
                    coldDownEnabled = false;
                    coldUpEnabled = false;
                    heatDownEnabled = false;
                    heatUpEnabled = false;
                    break;
                case Cool:
                    heatDownEnabled = false;
                    heatUpEnabled = false;
                    break;
                case Heat:
                    coldDownEnabled = false;
                    coldUpEnabled = false;
                    break;
                case Auto:
                    break;
            }

        if(coldDownEnabled) {
            cooldown.setVisibility(View.VISIBLE);
        } else {
            cooldown.setVisibility(View.INVISIBLE);
        }
        if(coldUpEnabled) {
            coolup.setVisibility(View.VISIBLE);
        } else {
            coolup.setVisibility(View.INVISIBLE);
        }
        if(heatDownEnabled) {
            heatdown.setVisibility(View.VISIBLE);
        } else {
            heatdown.setVisibility(View.INVISIBLE);
        }
        if(heatUpEnabled) {
            heatup.setVisibility(View.VISIBLE);
        } else {
            heatup.setVisibility(View.INVISIBLE);
        }

        if(mOperationMode == OperationMode.Hold) {
            mPermanentCool = mTargetTemperatureCool;
            mPermanentHeat = mTargetTemperatureHeat;
        } else {
            mTemporaryCool = mTargetTemperatureCool;
            mTemporaryHeat = mTargetTemperatureHeat;
        }

        boolean changed = false;
        if(mOperationMode != mOldMode) {
            changed = true;
        } else if(mOperationMode == OperationMode.Hold) {
            if((mOldPermanentHeat != mPermanentHeat) || (mOldPermanentCool != mPermanentCool)) {
                changed = true;
            }
        } else {

            if((mOldTemporaryCool != mTemporaryCool) || (mOldTemporaryHeat != mTemporaryHeat)) {
                changed = true;
            } else if(mHoldTimeEnable != mOldHoldTimeEnable) {
                changed = true;
            } else if((mSetMin != mOldSetMin) || (mSetHour != mOldSetHour)) {
                changed = true;
            }
        }

        Fragment f = getParentFragment();
        if (f instanceof ConferenceFragment) {
            ((ConferenceFragment) f).updateRightBtn(changed);
        }
    }

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.labelhold: {
                TimePickerDialog dialog = new TimePickerDialog(getActivity(), this, mSetHour, mSetMin, false);
                dialog.show();
                }
                break;
            case R.id.coolbtnup:
                if (mTargetTemperatureCool < Consts.TEMP_MAX) {
                    mTargetTemperatureCool++;
                    setTemperature(mCoolText, mTargetTemperatureCool);
                }

                updateView();
                break;
            case R.id.coolbtndown:
                if (mTargetTemperatureCool > mTargetTemperatureHeat) {
                    mTargetTemperatureCool--;
                    setTemperature(mCoolText, mTargetTemperatureCool);
                }
                updateView();
                break;
            case R.id.heatbtnup:
                if (mTargetTemperatureHeat < mTargetTemperatureCool) {
                    mTargetTemperatureHeat++;
                    setTemperature(mHeatText, mTargetTemperatureHeat);
                }
                updateView();
                break;
            case R.id.heatbtndown:
                if (mTargetTemperatureHeat > Consts.TEMP_MIN) {
                    mTargetTemperatureHeat--;
                    setTemperature(mHeatText, mTargetTemperatureHeat);
                }
                updateView();
                break;
        }
    }

    private void setTemperature(TextView textView, int temp) {
        Utils.setTemperature(getActivity(), textView, temp, mTemperatureUnit);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_tstat, container, false);
        mHeatText = (TextView) view.findViewById(R.id.heat_text_temperature);
        mCoolText = (TextView) view.findViewById(R.id.cool_text_temperature);
        mModeSwitch = (CheckBox) view.findViewById(R.id.switchmode);
        mModeTemperatureDesp = (TextView) view.findViewById(R.id.temperature_desp);
        mModeValue = (TextView) view.findViewById(R.id.modevalue);
        mLabelHold = (TextView) view.findViewById(R.id.labelhold);
        mLabelHold.setOnClickListener(this);

        coolup = view.findViewById(R.id.coolbtnup);
        cooldown = view.findViewById(R.id.coolbtndown);
        heatup = view.findViewById(R.id.heatbtnup);
        heatdown = view.findViewById(R.id.heatbtndown);

        coolup.setOnClickListener(this);
        cooldown.setOnClickListener(this);
        heatup.setOnClickListener(this);
        heatdown.setOnClickListener(this);
        mModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
                if (isChecked) {
                    mOperationMode = OperationMode.Schedule;
                } else {
                    mOperationMode = OperationMode.Hold;
                }
                refreshView(false);
                updateView();
            }
        });

        Bundle args = new Bundle(getArguments());
        mTstatValue = SnappyDBService.getInstance(getActivity()).getTstatValue(args);
        if(mTstatValue != null) {
            onUpdate(mTstatValue);
        }
        return view;
    }

    public void onUpdate(ThermostatValue tstatData) {
        mTemperatureUnit = tstatData.getTemperatureUnit();
        mTemporaryCool = tstatData.getTemporaryCool();
        mTemporaryHeat = tstatData.getTemporaryHeat();
        mPermanentCool = tstatData.getPermanentCool();
        mPermanentHeat = tstatData.getPermanentHeat();
        mCoolLimit     = tstatData.getCoolLimit();
        mHeatLimit     = tstatData.getHeatLimit();
        mTargetTemperatureCool = tstatData.getmTargetTemperatureCool();
        mTargetTemperatureHeat = tstatData.getmTargetTemperatureHeat();
        mOperationMode = tstatData.getOperationMode();
        mHoldTimeEnable = tstatData.isHoldTimeEnable();
        mHoldFor = tstatData.getHoldFor();

        Calendar now = Calendar.getInstance();
        if(mHoldTimeEnable) {
            now.add(Calendar.MINUTE, mHoldFor);
        }
        mSetHour = now.get(Calendar.HOUR_OF_DAY);
        mSetMin = now.get(Calendar.MINUTE);

        mOldMode = mOperationMode;
        mOldTemporaryCool = mTemporaryCool;
        mOldTemporaryHeat = mTemporaryHeat;
        mOldPermanentCool = mPermanentCool;
        mOldPermanentHeat = mPermanentHeat;
        mOldHoldFor = mHoldFor;
        mOldHoldTimeEnable = mHoldTimeEnable;
        mOldSetHour = mSetHour;
        mOldSetMin = mSetMin;
        refreshView(true);
        updateView();
    }

    private void refreshView(boolean update) {
        if (mOperationMode == OperationMode.Hold) {
            if (!update) {
                mTargetTemperatureCool = mPermanentCool;
                mTargetTemperatureHeat = mPermanentHeat;
            }
            mModeSwitch.setChecked(false);
            mModeValue.setText(R.string.label_hold);
            mModeTemperatureDesp.setText(R.string.label_temperature_permanent_hold);
            mLabelHold.setVisibility(View.GONE);
        } else {
            if (!update) {
                mTargetTemperatureCool = mTemporaryCool;
                mTargetTemperatureHeat = mTemporaryHeat;
            }
            mModeSwitch.setChecked(true);
            mModeValue.setText(R.string.label_schedule);
            setClock(mLabelHold);
            mLabelHold.setVisibility(View.VISIBLE);
        }
        setTemperature(mCoolText, mTargetTemperatureCool);
        setTemperature(mHeatText, mTargetTemperatureHeat);
    }

    private void setClock(TextView hold) {
        if(mHoldTimeEnable) {
            mModeTemperatureDesp.setText(R.string.label_temperature_temporary_hold);
            setClockText(hold, mSetHour, mSetMin);
        } else {
            mModeTemperatureDesp.setText(R.string.label_temperature_followschedle);
            mLabelHold.setText("Enable Temporary Hold");
        }

    }

    private void setClockText(TextView hold, int hour, int minu) {
        String hourText = hour < 10 ? "0" + hour : String.valueOf(hour);
        String minText = minu < 10 ? "0" + minu : String.valueOf(minu);
        String t = hold.getResources().getString(R.string.label_holdfor) + "  " + hourText + ":" + minText;
        hold.setText(t);
    }

    @Override
    public void onTimeSet(final TimePicker view, final int hourOfDay, final int minute) {
        if(hourOfDay == mSetHour && minute == mSetMin) {
            return;
        }

        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minu = now.get(Calendar.MINUTE);
        int holdfor =  (hourOfDay* 60 + minute) - (hour * 60 + minu);
        if(holdfor > 1) {
            mHoldFor = holdfor;
        } else if(holdfor < -1) {
            mHoldFor = 1440 + holdfor;
        } else {
            mHoldFor = 0;
        }

        mHoldTimeEnable = (mHoldFor != 0);
        mSetHour = hourOfDay;
        mSetMin = minute;
        setClock(mLabelHold);
        updateView();
    }
}
