package com.okser.android.thermostatx2.consts;

public enum OperationMode {
    Hold, Schedule;
    private static final OperationMode values[] = values();
    private static final int size = values().length;

    public static OperationMode get(int num) {
        if(num >= size) {
            num = 0;
        }
        return values[num];
    }
}
