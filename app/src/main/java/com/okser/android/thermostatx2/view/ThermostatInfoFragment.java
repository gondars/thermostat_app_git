package com.okser.android.thermostatx2.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.ThermostatValue;

public class ThermostatInfoFragment extends Fragment {

    public static ThermostatInfoFragment newInstance(int loc_pos, int tstat_pos) {

        Bundle args = new Bundle();
        args.putInt(Consts.KEY_LOCPOS, loc_pos);
        args.putInt(Consts.KEY_TSTATPOS, tstat_pos);
        ThermostatInfoFragment f = new ThermostatInfoFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(R.string.title_thermostat_info);

        View right = view.findViewById(R.id.btnright);
        right.setVisibility(View.GONE);
        View left = view.findViewById(R.id.btnleft);
        left.setVisibility(View.GONE);

        TextView vIP = ((TextView) view.findViewById(R.id.value_IP));
        TextView vID = ((TextView) view.findViewById(R.id.value_ID));
        TextView vSSID = ((TextView) view.findViewById(R.id.value_SSID));

        TextView btnDone = ((TextView) view.findViewById(R.id.btndone));
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        Bundle args = getArguments();
        if (args != null) {
            SnappyDBService db = SnappyDBService.getInstance(getActivity());
            ThermostatValue mTstatValue = db.getTstatValue(args);
            vIP.setText(mTstatValue.getIPaddress());
            vID.setText(mTstatValue.getMACAddr());
            vSSID.setText(mTstatValue.getWiFiName());
        }
        return view;
    }
}