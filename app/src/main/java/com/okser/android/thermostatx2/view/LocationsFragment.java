package com.okser.android.thermostatx2.view;

import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.adapter.LocationAdapter;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.Location;
import com.okser.android.thermostatx2.model.LocationValue;
import com.okser.android.thermostatx2.utils.NetworkUtil;

import java.util.List;

public class LocationsFragment extends Fragment implements View.OnClickListener {
    private ListView mListView;
    private LocationAdapter mLocationAdapter;
    private PopupWindow mPopupWindow;
    private ImageButton mRightBtn;
    private ImageButton mLeftBtn;
    private SnappyDBService mDB;

    private void showPopup(int pos) {
        hidePopup();
        View view = View.inflate(getActivity(), R.layout.popup, null);
        //mPopupWindow = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        double popUpSize = Resources.getSystem().getDisplayMetrics().widthPixels * 0.5;
        mPopupWindow = new PopupWindow(view,(int)popUpSize, (int)popUpSize, true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                mPopupWindow.dismiss();
                return true;
            }
        });
        mPopupWindow.showAtLocation(mListView, Gravity.BOTTOM, 0, (int)(Resources.getSystem().getDisplayMetrics().heightPixels * 0.1));

        //test

        View icon1 = view.findViewById(R.id.icon1);
        icon1.getLayoutParams().height = (int)(popUpSize * 0.35);
        icon1.getLayoutParams().width = (int)(popUpSize * 0.35);
        icon1.setPaddingRelative(0, 0, (int)(popUpSize * 0.1), 0);
        View icon2 = view.findViewById(R.id.icon2);
        icon2.getLayoutParams().height = (int)(popUpSize * 0.35);
        icon2.getLayoutParams().width = (int)(popUpSize * 0.35);
        icon2.setPaddingRelative((int)(popUpSize * 0.1), 0, 0, 0);
        View icon3 = view.findViewById(R.id.icon3);
        icon3.getLayoutParams().height = (int)(popUpSize * 0.35);
        icon3.getLayoutParams().width = (int)(popUpSize * 0.35);
        icon3.setPaddingRelative(0, 0, (int)(popUpSize * 0.1), 0);
        View icon4 = view.findViewById(R.id.icon4);
        icon4.getLayoutParams().height = (int)(popUpSize * 0.35);
        icon4.getLayoutParams().width = (int)(popUpSize * 0.35);
        icon4.setPaddingRelative((int)(popUpSize * 0.1), 0, 0, 0);
        icon1.setTag(pos);
        icon2.setTag(pos);
        icon3.setTag(pos);
        icon4.setTag(pos);
        icon1.setOnClickListener(this);
        icon2.setOnClickListener(this);
        icon3.setOnClickListener(this);
        icon4.setOnClickListener(this);
    }

    private void hidePopup() {
        if (mPopupWindow != null) {
            mPopupWindow.dismiss();
            mPopupWindow = null;
        }
    }

    @Override
    public void onClick(final View v) {
        MainActivity activity = (MainActivity) getActivity();
        int id = v.getId();
        if (id == R.id.btnright) {
            if (mLocationAdapter.isEdited()) {
                mLocationAdapter.save(activity);
                mRightBtn.setImageResource(R.drawable.icon_add);
            } else {
                // mDB.addLocation(new Location(activity.getString(R.string.default_location)));
                mDB.addLocation(new Location());
                mLocationAdapter.notifyDataSetChanged();
            }
        } else if (id == R.id.ic_next || id == R.id.righttext) {
            Integer tag = (Integer) v.getTag();
            Location location = mLocationAdapter.getItem(tag);
            activity.switchPage(ThermostatListFragment.newInstance(location.getName(), tag), true);
            /*
            if(location.getCount() == 0) {
                NewThermostatFragment f = NewThermostatFragment.newInstance(tag);
                //f.setDeviceFoundCallBack(mTstatUpdate);
                activity.switchPage(f, true);
            } else {
                activity.switchPage(ThermostatListFragment.newInstance(location.getName(), tag), true);
            }
            */
        } else if (id == R.id.btnleft) {
            mDB.delCheckedLocation();
            mLeftBtn.setEnabled(false);
            mLeftBtn.setVisibility(View.INVISIBLE);
            mLocationAdapter.notifyDataSetChanged();
        } else if (id == R.id.delCheck) {
            mDB.toggleDelCheck((Integer) v.getTag());
            if(mDB.isDelChecked()) {
                mLeftBtn.setVisibility(View.VISIBLE);
                mLeftBtn.setEnabled(true);
            } else {
                mLeftBtn.setEnabled(false);
                mLeftBtn.setVisibility(View.INVISIBLE);
            }

        } else if (id == R.id.icon) {
            showPopup((Integer) v.getTag());
        } else if (id == R.id.icon1) {
            Location location = mLocationAdapter.getItem((Integer) v.getTag());
            location.setIconId(R.drawable.popup_icon1);
            mDB.updateLocation();
            hidePopup();
            mLocationAdapter.notifyDataSetChanged();
        } else if (id == R.id.icon2) {
            Location location = mLocationAdapter.getItem((Integer) v.getTag());
            location.setIconId(R.drawable.popup_icon2);
            mDB.updateLocation();
            hidePopup();
            mLocationAdapter.notifyDataSetChanged();
        } else if (id == R.id.icon3) {
            Location location = mLocationAdapter.getItem((Integer) v.getTag());
            location.setIconId(R.drawable.popup_icon3);
            mDB.updateLocation();
            hidePopup();
            mLocationAdapter.notifyDataSetChanged();
        } else if (id == R.id.icon4) {
            Location location = mLocationAdapter.getItem((Integer) v.getTag());
            location.setIconId(R.drawable.popup_icon4);
            mDB.updateLocation();
            hidePopup();
            mLocationAdapter.notifyDataSetChanged();
        } else if (id == R.id.lefttext) {
            if (mLocationAdapter.isEdited()) {
                mRightBtn.setImageResource(R.drawable.icon_ok);
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDB = SnappyDBService.getInstance(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_loclistview, container, false);
        int bgSize = (int)(Resources.getSystem().getDisplayMetrics().heightPixels * 0.7);
        ImageView bgIcon = (ImageView) view.findViewById(R.id.location_bg);
        bgIcon.getLayoutParams().height = bgSize;
        mListView = (ListView) view.findViewById(R.id.loclistview);
        mLocationAdapter = new LocationAdapter(getActivity(), this);
        mListView.setAdapter(mLocationAdapter);
        TextView title = (TextView) view.findViewById(R.id.title);
        title.setText(R.string.title_locations);
        mLeftBtn = (ImageButton) view.findViewById(R.id.btnleft);
        mLeftBtn.setVisibility(View.GONE);
        mLeftBtn.setEnabled(false);
        mLeftBtn.setOnClickListener(this);
        mRightBtn = (ImageButton) view.findViewById(R.id.btnright);
        mRightBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(final View view, final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mListView.requestFocus();
    }

    @Override
    public void onResume() {
        super.onResume();
        String wifiName = NetworkUtil.getWifiName(getActivity());
        if(wifiName == null) {
            return;
        }

        mDB.setmCrtSSID(wifiName);
        List<Location> LocationList = mDB.getLocationList();
        int pos = 0;
        for(Location l : LocationList) {
            if(wifiName.equals(l.getSsid())) {
                LocationValue lv = mDB.getLocationValue(pos);
                lv.MDNSScan(false);
            }
            pos++;
        }
    }

}
