package com.okser.android.thermostatx2.view;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.Consts;
import com.okser.android.thermostatx2.consts.System;
import com.okser.android.thermostatx2.db.SnappyDBService;
import com.okser.android.thermostatx2.model.ThermostatValue;
import com.okser.android.thermostatx2.utils.ITabListener;
import com.okser.android.thermostatx2.utils.Utils;


public class ConferenceFragment extends Fragment implements View.OnClickListener {
    private ThermostatValue mTstatValue;
    private int mCurrentTab;
    private int mPendingId;
    private View mProgressBar;
    private ImageButton mRightBtn;
    private View mTab_sched;
    private View mTab_system;
    private View mTab_tstat;
    private TextView mTitleText;
    private int progressCnt;

    private RelativeLayout topBar;
    private RelativeLayout botBar;
    private ImageView botLine;

    private Handler mHandler;

    public static ConferenceFragment newInstance(Bundle args) {
        ConferenceFragment f = new ConferenceFragment();
        f.setArguments(args);
        return f;
    }

    public static ConferenceFragment newInstance(int loc_pos, int tstat_pos) {
        Bundle args = new Bundle();
        args.putInt(Consts.KEY_LOCPOS, loc_pos);
        args.putInt(Consts.KEY_TSTATPOS, tstat_pos);
        return newInstance(args);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Add delay
        mHandler.postDelayed(mRunnable, 2000);
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(mRunnable);
    }

    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            Log.i("ConferenceFragment", "run");

            if(mTstatValue != null) {
                mTstatValue.onUpdate(new ThermostatValue.DataSetChangedListener() {
                    @Override
                    public void DataSetChanged(ChangedType c, boolean configUpdate) {
                        if(configUpdate) {
                            mHandler.removeCallbacks(mRunnable);
                            mHandler.postDelayed(mRunnable, 10000);
                        }
                        if (c == ChangedType.STATUS) {

                            if (progressCnt >= 3) {
                                // Config update fail.
                                mProgressBar.setVisibility(View.GONE);
                                progressCnt = 0;
                                Toast.makeText(getActivity(), "Send data fail", Toast.LENGTH_SHORT).show();
                            }
                        } else if (c == ChangedType.GET_CONFIG) {
                            if (progressCnt > 0) {
                                mProgressBar.setVisibility(View.GONE);
                                progressCnt = 0;
                                afterSave();
                            }
                            Fragment f = getChildFragmentManager().findFragmentById(R.id.tabcontent);
                            if (f instanceof ITabListener) {
                                ((ITabListener) f).onUpdate(mTstatValue);
                            }
                        } else {
                            mProgressBar.setVisibility(View.GONE);
                        }
                    }
                });
            }
            if (mProgressBar.getVisibility() == View.VISIBLE) {
                progressCnt++;
                if (progressCnt >= 4) {
                    // Config update fail.
                    mProgressBar.setVisibility(View.GONE);
                    progressCnt = 0;
                    Toast.makeText(getActivity(), "Send data fail", Toast.LENGTH_SHORT).show();
                }
                mHandler.postDelayed(mRunnable, 10000);
            } else {
                mHandler.postDelayed(mRunnable, 30000);
            }

        }
    };

    private void updateChange() {
        Fragment f = getChildFragmentManager().findFragmentById(R.id.tabcontent);
        if (f instanceof ITabListener) {
            mProgressBar.setVisibility(View.VISIBLE);
            progressCnt = 0;
            mHandler.removeCallbacks(mRunnable);
            ((ITabListener) f).doSave(new ThermostatValue.DataSetChangedListener() {
                @Override
                public void DataSetChanged(ChangedType c, boolean configUpdate) {
                    mHandler.postDelayed(mRunnable, 5000);
                }
            });

        }
    }

    private void showConfirmSave(final int pendingId) {
        mPendingId = pendingId;
        Utils.showSubmitConfirm(getActivity(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    updateChange();
                } else {
                    afterSave();
                }
            }
        });
    }

    public boolean doBackAction() {
        Fragment f = getChildFragmentManager().findFragmentById(R.id.tabcontent);
        if (f instanceof ITabListener) {
            if (((ITabListener) f).doBackAction()) {
                if (mRightBtn.getVisibility() == View.VISIBLE) {
                    showConfirmSave(-1);
                    return false;
                }
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    @Override
    public void onClick(final View v) {
        if (v.getId() == R.id.btnleft) {
            if (doBackAction()) {
                getFragmentManager().popBackStack();
            }
        } else if (v.getId() == R.id.btnright) {
            updateChange();
        // } else if (v.getId() == R.id.progress) {
        } else {
            if (mCurrentTab != v.getId()) {
                if (mRightBtn.getVisibility() == View.VISIBLE) {
                    showConfirmSave(v.getId());
                } else {
                    switchViewById(v.getId());
                }
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conference, container, false);
        mProgressBar = view.findViewById(R.id.progress);
        mProgressBar.setVisibility(View.GONE);

        topBar = (RelativeLayout) view.findViewById(R.id.titlebar);
        botBar = (RelativeLayout) view.findViewById(R.id.tabs);
        botLine = (ImageView) view.findViewById(R.id.bot_line);

        mRightBtn = (ImageButton) view.findViewById(R.id.btnright);
        mRightBtn.setImageResource(R.drawable.icon_ok);
        mRightBtn.setVisibility(View.GONE);
        mRightBtn.setOnClickListener(this);
        mTitleText = (TextView) view.findViewById(R.id.title);

        View left = view.findViewById(R.id.btnleft);
        left.setOnClickListener(this);
        mTab_tstat = view.findViewById(R.id.tab_tstat);
        mTab_sched = view.findViewById(R.id.tab_setsched);
        mTab_system = view.findViewById(R.id.tab_system);
        mTab_tstat.setOnClickListener(this);
        mTab_sched.setOnClickListener(this);
        mTab_system.setOnClickListener(this);

        mTstatValue = SnappyDBService.getInstance(getActivity()).getTstatValue(getArguments());
        mCurrentTab = -1;
        String name = mTstatValue.getName();
        mTitleText.setText(name);
        if (mTstatValue.getSystem() == System.Off) {
            switchViewById(R.id.tab_system);
        } else {
            switchViewById(R.id.tab_tstat);
        }
        return view;
    }

    private void switchViewById(int id) {
        mRightBtn.setVisibility(View.INVISIBLE);
        mCurrentTab = id;
        Bundle args = getArguments();
        if (id == R.id.tab_setsched) {
            mTab_sched.setSelected(true);
            mTab_system.setSelected(false);
            mTab_tstat.setSelected(false);
            mTab_tstat.setEnabled(true);
            mTab_sched.setEnabled(false);
            mTab_system.setEnabled(true);
            switchPage(SetSchedFragment.newInstance(args), false);
            topBar.setBackgroundColor(Color.parseColor("#21002f"));
            botBar.setBackgroundColor(Color.parseColor("#4c2c94"));
            botLine.setBackgroundColor(Color.parseColor("#21002f"));
        } else if (id == R.id.tab_system) {
            mTab_sched.setSelected(false);
            mTab_system.setSelected(true);
            mTab_tstat.setSelected(false);
            mTab_tstat.setEnabled(true);
            mTab_sched.setEnabled(true);
            mTab_system.setEnabled(false);
            switchPage(SystemFragment.newInstance(args), false);
            topBar.setBackgroundColor(Color.parseColor("#15003f"));
            botBar.setBackgroundColor(Color.parseColor("#2c499b"));
            botLine.setBackgroundColor(Color.parseColor("#15003f"));
        } else if (id == R.id.tab_tstat) {
            mTab_sched.setSelected(false);
            mTab_system.setSelected(false);
            mTab_tstat.setSelected(true);
            mTab_tstat.setEnabled(false);
            mTab_sched.setEnabled(true);
            mTab_system.setEnabled(true);
            switchPage(TStatFragment.newInstance(args), false);
            topBar.setBackgroundColor(Color.parseColor("#002f3f"));
            botBar.setBackgroundColor(Color.parseColor("#078988"));
            botLine.setBackgroundColor(Color.parseColor("#002f3f"));
        }
    }

    public void switchPage(Fragment fragment, boolean add) {
        FragmentManager fragmentManager = this.getChildFragmentManager();
        int fragmentCount = fragmentManager.getBackStackEntryCount();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.tabcontent, fragment, Integer.toString(fragmentCount));
        if (add) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public void afterSave() {
        if (mPendingId == -1) {
            getFragmentManager().popBackStack();
        } else if (mPendingId != mCurrentTab) {
            switchViewById(mPendingId);
        }
        mPendingId = -2;
    }

    public void setTitle(final String value) {
        if (TextUtils.isEmpty(value)) {
            String name = mTstatValue.getName();
            mTitleText.setText(name);
        } else {
            mTitleText.setText(value);
        }
    }

    public void updateRightBtn(final boolean show) {
        mRightBtn.setEnabled(show);
        mRightBtn.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }
}
