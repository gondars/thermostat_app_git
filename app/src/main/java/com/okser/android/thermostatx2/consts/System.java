package com.okser.android.thermostatx2.consts;

public enum System {
    Off, Cool, Heat, Auto;
    private static final System values[] = values();
    private static final int size = values().length;

    public static System get(int num) {
        if(num >= size) {
            num = 0;
        }
        return values[num];
    }
}
