package com.okser.android.thermostatx2.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.okser.android.thermostatx2.R;
import com.okser.android.thermostatx2.consts.TemperatureUnit;

import org.json.JSONObject;

public class Utils {


    public static void hideKeyboard(Context ctx, View view) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void setTemperature(Context ctx, TextView view, int temp, TemperatureUnit temperatureUnit) {
        if (temperatureUnit == TemperatureUnit.F) {
            view.setText(String.format(ctx.getString(R.string.unit_f), temp));
        } else {
            view.setText(String.format(ctx.getString(R.string.unit_c), temp));
        }
    }

    public static void showKeyboard(Context ctx, View view) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, 0);
    }

    public static void showSubmitConfirm(final Context ctx, final DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(R.string.dialog_change);
        builder.setMessage(R.string.dialog_submit);
        builder.setPositiveButton(R.string.btn_ok, listener);
        builder.setNegativeButton(R.string.btn_cancel, listener);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static JsonObjectRequest volleyJsonRequest(String url,
                                                      Response.Listener<JSONObject> listener, Response.ErrorListener errorListener) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, listener, errorListener);
        return jsonObjectRequest;
    }
}
