package com.okser.android.thermostatx2.utils;

import com.okser.android.thermostatx2.model.ThermostatValue;

public interface ITabListener {
    boolean doBackAction();

    void doSave(final ThermostatValue.DataSetChangedListener listener);

    void onUpdate(ThermostatValue tstatData);

}
